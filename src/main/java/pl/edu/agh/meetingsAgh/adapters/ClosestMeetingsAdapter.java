package pl.edu.agh.meetingsAgh.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.activities.MainActivity;
import pl.edu.agh.meetingsAgh.activities.MeetingActivity;
import pl.edu.agh.meetingsAgh.entity.Meeting;

/**
 * List adapter created for closest meetings list in subject and group details.
 */
public class ClosestMeetingsAdapter extends ArrayAdapter<Meeting> {
	private final Activity context;
	private boolean isGroup = false;
	private long groupId;

	public ClosestMeetingsAdapter(Activity context){
		super(context, R.layout.closest_meeting_item);
		this.context = context;
	}

	public ClosestMeetingsAdapter(Activity context, long groupId){
		this(context);
		this.isGroup = true;
		this.groupId = groupId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.closest_meeting_item, parent, false);
		assert rowView != null;
		TextView dayView = (TextView)rowView.findViewById(R.id.day);
		TextView hourView = (TextView)rowView.findViewById(R.id.hour);
		Button seeButton = (Button)rowView.findViewById(R.id.see);
		final Meeting meeting = getItem(position);

		// Set proper day in format "Friday, 12.12.2012"
		String dayText = String.format("%s, %s",
			DateUtils.formatDateTime(context, meeting.getStartTime(), DateUtils.FORMAT_SHOW_WEEKDAY),
			DateUtils.formatDateTime(context, meeting.getStartTime(), DateUtils.FORMAT_SHOW_DATE)
		);
		dayView.setText(dayText);

		// Set proper hours in format "10:00 - 12:00"
		String hourText = String.format("%s - %s",
			DateUtils.formatDateTime(context, meeting.getStartTime(), DateUtils.FORMAT_SHOW_TIME),
			DateUtils.formatDateTime(context, meeting.getEndTime(), DateUtils.FORMAT_SHOW_TIME)
		);
		hourView.setText(hourText);

		// Add click listener to open meeting view
		seeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v){
				Intent intent = new Intent(context, MeetingActivity.class);
				intent.putExtra(MeetingActivity.MEETING, meeting);

				if(isGroup){
					intent.putExtra(MeetingActivity.GROUP, groupId);
				}

				context.startActivityForResult(intent, MainActivity.REFRESH_MEETING_DAY);
			}
		});

		return rowView;
	}
}
