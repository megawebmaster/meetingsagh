package pl.edu.agh.meetingsAgh.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.Subject;

public class SubjectListAdapter extends ArrayAdapter<Subject> {
	public SubjectListAdapter(Context context){
		super(context, R.layout.subjects_item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.subjects_item, parent, false);
		assert rowView != null;
		TextView subjectView = (TextView)rowView.findViewById(R.id.subject);
		TextView descriptionView = (TextView)rowView.findViewById(R.id.subject_description);
		final Subject subject = getItem(position);

		subjectView.setText(subject.getName());
		descriptionView.setText(String.format(
			getContext().getText(R.string.subject_description).toString(),
			subject.getStudentList().size()
		));

		return rowView;
	}
}
