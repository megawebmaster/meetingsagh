package pl.edu.agh.meetingsAgh.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.Group;

public class GroupListAdapter extends ArrayAdapter<Group> {
	public GroupListAdapter(Context context){
		super(context, R.layout.groups_item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.groups_item, parent, false);
		assert rowView != null;
		TextView groupView = (TextView)rowView.findViewById(R.id.group);
		TextView descriptionView = (TextView)rowView.findViewById(R.id.group_description);
		final Group group = getItem(position);

		groupView.setText(group.getName());
		descriptionView.setText(String.format(
			getContext().getText(R.string.group_description).toString(),
			group.getStudentList().size()
		));

		return rowView;
	}
}
