package pl.edu.agh.meetingsAgh;

public class Constants {
	/** Account type string. */
	public static final String ACCOUNT_TYPE = "pl.edu.agh.meetingsAgh.account";

	/** Authtoken type string. */
	public static final String AUTHTOKEN_TYPE = "pl.edu.agh.meetingsAgh";

	/** Service URI. */
	public static final String SERVICE_URI = "jagular.iisg.agh.edu.pl:60280/meetings";

	/** Service URI. */
	public static final String OAUTH_CLIENT_ID = "android_app";

	/** Service URI. */
	public static final String OAUTH_CLIENT_SECRET = "meetings_agh";

	/** Shared preferences name. */
	public static final String PREFERENCES_KEY = "MeetingsAGH";

	/** Username value stored in preferences key. */
	public static final String PREFERENCES_USERNAME = "username";

	/** Number of downloaded closest meetings in subject and group details view. */
	public static final int CLOSEST_MEETINGS_COUNT = 5;
}
