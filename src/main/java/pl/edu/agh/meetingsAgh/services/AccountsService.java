package pl.edu.agh.meetingsAgh.services;

import android.accounts.*;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.common.AccountPicker;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.User;

import java.io.IOException;

public class AccountsService {
	/** Request code for account picking. */
	public static final int REQUEST_PICK_ACCOUNT = 1;

	private static final String TAG = "MeetingsAGH AccountsService";

	private final Activity parent;
	private final AccountManager manager;

	public AccountsService(FragmentActivity parent, AccountManager manager){
		this.parent = parent;
		this.manager = manager;
	}

	public boolean isCurrentUser(User user){
		return user.getLogin().equals(getAccount().name);
	}

	public String getCurrentUser(){
		SharedPreferences preferences = parent.getSharedPreferences(Constants.PREFERENCES_KEY, Activity.MODE_PRIVATE);
		return preferences.getString(Constants.PREFERENCES_USERNAME, null);
	}

	public void showAccountSelector(){
		// Show select account dialog
		Intent accountPicker = AccountPicker.newChooseAccountIntent(null, null, new String[]{Constants.ACCOUNT_TYPE}, true, null,
			Constants.AUTHTOKEN_TYPE, null, null);
		parent.startActivityForResult(accountPicker, REQUEST_PICK_ACCOUNT);
	}

	public void selectAccount(){
		Account[] accounts = manager.getAccountsByType(Constants.ACCOUNT_TYPE);
		switch(accounts.length){
			case 0:
				// Show login screen
				manager.addAccount(Constants.ACCOUNT_TYPE, Constants.AUTHTOKEN_TYPE, null, new Bundle(), parent, new OnAddAccountCallback(), new Handler(new OnAddAccountError()));
				break;
			default:
				String currentUser = getCurrentUser();

				if(currentUser != null){
					setAccount(findUserAccount(currentUser));
				} else {
					showAccountSelector();
				}
				break;
		}
	}

	/**
	 * Sets up currently used user account and saves its username in preferences.
	 *
	 * @param account Account to use.
	 */
	public void setAccount(Account account){
		SharedPreferences.Editor editor = parent.getSharedPreferences(Constants.PREFERENCES_KEY, Activity.MODE_PRIVATE).edit();
		editor.putString(Constants.PREFERENCES_USERNAME, account.name);
		editor.commit();
	}

	/**
	 * Set up currently used user account.
	 *
	 * @param accountName Account name.
	 */
	public void setAccount(String accountName){
		setAccount(findUserAccount(accountName));
	}

	/**
	 * Returns currently used account.
	 *
	 * @return Currently selected account or null if not selected.
	 */
	public Account getAccount(){
		SharedPreferences preferences = parent.getSharedPreferences(Constants.PREFERENCES_KEY, Activity.MODE_PRIVATE);
		String currentUser = preferences.getString(Constants.PREFERENCES_USERNAME, null);

		if(currentUser != null){
			return findUserAccount(currentUser);
		}

		return null;
	}

	/**
	 * Unset currently used user account and removes its username from preferences.
	 */
	public void unsetAccount(){
		SharedPreferences.Editor editor = parent.getSharedPreferences(Constants.PREFERENCES_KEY, Activity.MODE_PRIVATE).edit();
		editor.remove(Constants.PREFERENCES_USERNAME);
		editor.commit();
	}

	/**
	 * Finds account by username.
	 *
	 * @param username Username.
	 * @return Account found or null.
	 */
	public Account findUserAccount(String username){
		Account[] accounts = manager.getAccountsByType(Constants.ACCOUNT_TYPE);
		for(Account account : accounts){
			if(account.name.equals(username)){
				return account;
			}
		}

		return null;
	}

	private class OnAddAccountCallback implements AccountManagerCallback<Bundle> {
		@Override
		public void run(AccountManagerFuture<Bundle> future){
			try {
				String accountName = future.getResult().getString(AccountManager.KEY_ACCOUNT_NAME);
				Log.d(TAG, "OnAddAccountCallback: account name: " + accountName);

				Account[] accounts = manager.getAccountsByType(Constants.ACCOUNT_TYPE);
				for(Account account : accounts){
					if(account.name.equals(accountName)){
						setAccount(account);
						break;
					}
				}
			} catch(OperationCanceledException e){
				parent.finish();
			} catch(IOException e){
				Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
			} catch(AuthenticatorException e) {
				Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
			}
		}
	}
	private class OnAddAccountError implements Handler.Callback {
		@Override
		public boolean handleMessage(Message msg){
			Toast.makeText(parent, msg.toString(), Toast.LENGTH_LONG).show();
			return false;
		}
	}
}
