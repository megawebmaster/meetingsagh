package pl.edu.agh.meetingsAgh.ui.widget;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import android.view.View;
import android.widget.Button;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.activities.MainActivity;
import pl.edu.agh.meetingsAgh.activities.MeetingActivity;
import pl.edu.agh.meetingsAgh.entity.Meeting;

import java.text.ParseException;
import java.util.List;

/**
 * Custom view that represents a {@link pl.edu.agh.meetingsAgh.entity.Meeting} instance, including its
 * title and time span that it occupies. Usually organized automatically by
 * {@link BlocksLayout} to match up against a {@link TimeRulerView} instance.
 */
public class MeetingButtonView extends Button {
	private final long startTime;
	private final long endTime;

	private int column = 0;
	private int columns = 1;
	private List<MeetingButtonView> siblings;
	private Meeting meeting;

	public MeetingButtonView(final Activity context, final Meeting meeting) throws ParseException {
		super(context);

		this.meeting = meeting;
		this.startTime = meeting.getStartTime();
		this.endTime = meeting.getEndTime();
		String title = meeting.getSubjectList().isEmpty() ? meeting.getAuthor().toString() : Joiner.on(", ").join(meeting.getSubjectList());
		setText(title);

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v){
				Intent intent = new Intent(context, MeetingActivity.class);
				intent.putExtra(MeetingActivity.MEETING, meeting);
				context.startActivityForResult(intent, MainActivity.REFRESH_MEETING_DAY);
			}
		});

		// Set default values
		setBackgroundColor(Color.DKGRAY);
		setTextColor(Color.BLACK);
		setTextSize(14f);

		Resources resources = getResources();

		if(resources != null){
			int color = Color.parseColor(meeting.getColor());

			// Fetch drawable
			StateListDrawable background = new StateListDrawable();
			// Get basic parameters for buttons
			GradientDrawable button = (GradientDrawable)resources.getDrawable(R.drawable.meeting_button);
			assert button != null;

			// If luminosity is higher switch text color to black
			if(getPerceivedBrightness(Color.red(color), Color.blue(color), Color.green(color)) < 140){
				setTextColor(Color.WHITE);
				button.setStroke((int)resources.getDimension(R.dimen.meeting_button_stroke), Color.WHITE);
			}

			// Add pressed state
			background.addState(new int[]{ android.R.attr.state_pressed}, button);
			// Update its color
			button = (GradientDrawable)button.mutate();
			button.setColor(color);

			background.addState(StateSet.WILD_CARD, button);
			//noinspection deprecation
			setBackgroundDrawable(background);
		}
	}

	public long getStartTime(){
		return startTime;
	}

	public long getEndTime(){
		return endTime;
	}

	public int getColumn(){
		return column;
	}

	public void setColumn(int column){
		this.column = column;
	}

	public int getColumns(){
		return columns;
	}

	public boolean setColumns(int columns){
		if(columns > this.columns){
			this.columns = columns;
			return true;
		}

		return false;
	}

	public List<MeetingButtonView> getSiblings(){
		return siblings;
	}

	public void setSiblings(List<MeetingButtonView> siblings){
		this.siblings = siblings;
	}

	public Meeting getMeeting(){
		return meeting;
	}

	public String toString(){
		return String.format("Start: %d, End: %d, Column: %d", startTime, endTime, column);
	}

	/**
	 * Returns perceived brightness of selected color.
	 *
	 * @param red Red value.
	 * @param blue Blue value.
	 * @param green Green value.
	 * @return Calculated brightness.
	 */
	private int getPerceivedBrightness(int red, int blue, int green){
		return (int)Math.sqrt(
			red * red * .241 +
			green * green * .691 +
			blue * blue * .068
		);
	}
}
