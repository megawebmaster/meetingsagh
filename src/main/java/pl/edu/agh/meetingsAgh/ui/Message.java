package pl.edu.agh.meetingsAgh.ui;

public class Message {
	private boolean ok;
	private boolean twoMessage;
	private String message;
	private String messageTwo;

	public boolean isOk(){
		return ok;
	}

	public boolean isTwoMessage(){
		return twoMessage;
	}

	public String getMessage(){
		return message;
	}

	public String getMessageTwo(){
		return messageTwo;
	}
}