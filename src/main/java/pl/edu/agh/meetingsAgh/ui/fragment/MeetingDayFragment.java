package pl.edu.agh.meetingsAgh.ui.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.joda.time.LocalDate;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.Meeting;
import pl.edu.agh.meetingsAgh.entity.MeetingList;
import pl.edu.agh.meetingsAgh.request.MeetingListRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;
import pl.edu.agh.meetingsAgh.ui.widget.BlocksLayout;
import pl.edu.agh.meetingsAgh.ui.widget.MeetingButtonView;
import pl.edu.agh.meetingsAgh.ui.widget.ObservableScrollView;

import java.io.Serializable;
import java.text.ParseException;

public class MeetingDayFragment extends Fragment implements AppFragment, ParentAwareFragment, Serializable {
	private static final String TAG = "MeetingsAGH MeetingDayFragment";
	private static final String START_DATE = "start_date";
	private static final String MEETING_LIST = "meeting_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;

	private TextView title;
	private BlocksLayout blocks;
	private ObservableScrollView scrollView;

	private MeetingList meetingList;
	private LocalDate startDate;
	private LocalDate endDate;

	private int retryCount = 1;
	private Fragment parentFragment;
	private Menu menu;

	// Used when saved instance is restored
	public MeetingDayFragment(){
		progress = new RESTCallProgressDialogFragment(MeetingList.class, R.string.meetings_fragment_loading);
	}

	public MeetingDayFragment(LocalDate startDate){
		this();
		this.startDate = startDate;
		this.endDate = startDate.plusDays(1);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getActivity().getApplicationContext());

		if(savedInstanceState != null){
			// Restore state
			startDate = new LocalDate(savedInstanceState.getLong(START_DATE));
			endDate = startDate.plusDays(1);
			meetingList = (MeetingList)savedInstanceState.getSerializable(MEETING_LIST);
		}

		setHasOptionsMenu(true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putLong(START_DATE, startDate.toDate().getTime());
		outState.putSerializable(MEETING_LIST, meetingList);
	}

	@Override
	public void onResume(){
		super.onResume();
		accountsService = new AccountsService(getActivity(), accountManager);
		updatePage();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup)inflater.inflate(R.layout.meetings_day, null);

		assert root != null;
		scrollView = (ObservableScrollView)root.findViewById(R.id.blocks_scroll);
		blocks = (BlocksLayout)root.findViewById(R.id.blocks);
		title = (TextView)root.findViewById(R.id.block_title);

		// Setup scroll view
		if(parentFragment != null && parentFragment instanceof ObservableScrollView.OnScrollListener){
			scrollView.setOnScrollListener((ObservableScrollView.OnScrollListener)parentFragment);
		}

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getActivity().getApplicationContext());
		createMenu();
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		this.menu = menu;
		createMenu();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.refresh:
				refresh();
				updatePage();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void refresh(){
		meetingList = null;
	}

	/**
	 * Updates page contents if needed.
	 */
	public void updatePage(){
		updateTitle();
		loadMeetings();
	}

	@Override
	public void setParentFragment(Fragment parent){
		parentFragment = parent;
	}

	public void createMenu(){
		if(menu != null){
			MenuItem refresh;
			if(menu.findItem(R.id.refresh) != null){
				menu.removeItem(R.id.refresh);
			}

			refresh = menu.add(Menu.NONE, R.id.refresh, 10, R.string.refresh);
			refresh.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			refresh.setIcon(R.drawable.ic_action_refresh);
		}
	}

	/**
	 * @return Observable scroll view associated with the fragment.
	 */
	public ObservableScrollView getScrollView(){
		return scrollView;
	}

	private void updateTitle(){
		if(title != null){
			title.setText(startDate.toString());
		}
	}

	private void showPage(){
		blocks.removeAllBlocks();

		for(Meeting meeting : meetingList){
			try {
				blocks.addBlock(new MeetingButtonView(getActivity(), meeting));
			} catch(ParseException e){
				Log.i(TAG, String.format("Invalid date format. Exception: %s", e.toString()));
			}
		}

		blocks.calculatePositions();
	}

	private void loadMeetings(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(meetingList == null){
				Request<MeetingList> request = new MeetingListRequest(startDate.toDate(), endDate.toDate(), accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getFragmentManager());
				accountManager.getAuthToken(account, Constants.AUTHTOKEN_TYPE, new Bundle(), getActivity(),
					new OnTokenAcquired<MeetingList>(getActivity(), spiceManager, request, new MeetingListRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(getActivity()))
				);
			} else {
				showPage();
			}
		}
	}

	private class MeetingListRequestListener implements RequestListener<MeetingList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadMeetings() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadMeetings();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(getActivity(), R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(MeetingList meetings){
			Log.d(TAG, String.format("Meetings count: %d", meetings.size()));
			progress.dismiss(getFragmentManager());
			meetingList = meetings;
			showPage();
		}
	}
}
