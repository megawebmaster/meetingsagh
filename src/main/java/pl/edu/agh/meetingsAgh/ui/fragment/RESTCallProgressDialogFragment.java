package pl.edu.agh.meetingsAgh.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Dialog with progress circle and message.
 */
public class RESTCallProgressDialogFragment extends DialogFragment {
	private static final String TAG = "MeetingsAGH RESTCallProgressDialogFragment";
	private static final String MESSAGE_ID = "message_id";
	private static final String CLAZZ = "clazz";
	private static final String CACHE_KEY = "cache_key";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private int messageId;
	private Class clazz;
	private String cacheKey;

	/**
	 * Required empty constructor.
	 */
	public RESTCallProgressDialogFragment(){}

	/**
	 * Constructor.
	 *
	 * @param clazz Class of REST call result.
	 * @param messageId Message ID to display.
	 */
	public RESTCallProgressDialogFragment(Class clazz, int messageId){
		this.messageId = messageId;
		this.clazz = clazz;
	}

	@Override
	public Dialog onCreateDialog(Bundle args) {
		Activity activity = getActivity();
		assert activity != null;

		if(args != null){
			// Restore state
			clazz = (Class)args.getSerializable(CLAZZ);
			messageId = args.getInt(MESSAGE_ID);
			cacheKey = args.getString(CACHE_KEY);
		}

		final ProgressDialog dialog = new ProgressDialog(activity);
		dialog.setMessage(getText(messageId));
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		return dialog;
	}

	@Override
	public void onStart(){
		super.onStart();
		spiceManager.start(getActivity());
	}

	@Override
	public void onStop(){
		super.onStop();
		spiceManager.shouldStop();
	}

	@Override
	public void onCancel(DialogInterface dialog){
		super.onCancel(dialog);

		Log.i(TAG, String.format("User cancelled action: '%s'.", getText(messageId)));
		spiceManager.cancel(clazz, cacheKey);
		dismiss();
	}

	@Override
	public void onSaveInstanceState(Bundle outState){
		outState.putInt(MESSAGE_ID, messageId);
		outState.putSerializable(CLAZZ, clazz);
		outState.putString(CACHE_KEY, cacheKey);
		super.onSaveInstanceState(outState);
	}

	public void setCacheKey(String cacheKey){
		this.cacheKey = cacheKey;
	}

	/**
	 * Shows progress dialog. If needed - dismisses earlier added progress dialog.
	 *
	 * @param manager Manager to show progress bar.
	 */
	public void show(FragmentManager manager){
		String key = getProgressDialogKey();
		Fragment frag = manager.findFragmentByTag(key);

		if(frag != null){
			((DialogFragment)frag).dismiss();
		}

		try {
			show(manager, key);
		} catch(IllegalStateException e){
			Log.v(TAG, "Skipping progress dialog.");
		}
	}

	public void dismiss(FragmentManager manager){
		Fragment frag = manager.findFragmentByTag(getProgressDialogKey());

		if(frag != null){
			((DialogFragment)frag).dismiss();
		} else if(isAdded()) {
			super.dismiss();
		}
	}

	private String getProgressDialogKey(){
		return String.format("progress_%s", cacheKey);
	}
}
