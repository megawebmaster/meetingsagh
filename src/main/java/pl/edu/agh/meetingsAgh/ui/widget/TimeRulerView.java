package pl.edu.agh.meetingsAgh.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.View;
import pl.edu.agh.meetingsAgh.R;

public class TimeRulerView extends View {
	private int headerWidth;
	private int hourHeight;
	private int labelTextSize;
	private int labelPaddingTop = 2;
	private int labelPaddingLeft = 8;
	private int labelColor = Color.BLACK;
	private int dividerColor = Color.LTGRAY;
	private int startHour = 8;
	private int endHour = 22;

	public TimeRulerView(Context context) {
		this(context, null);
	}

	public TimeRulerView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TimeRulerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		headerWidth = (int)context.getResources().getDimension(R.dimen.meetings_header_width);
		hourHeight = (int)context.getResources().getDimension(R.dimen.meetings_hour_size);
		labelTextSize = (int)context.getResources().getDimension(R.dimen.meetings_hour_text_size);
	}

	/**
	 * Return the vertical offset (in pixels) for a requested time (in
	 * milliseconds since epoch).
	 */
	public int getTimeVerticalOffset(long timeMillis) {
		Time time = new Time();
		time.set(timeMillis);

		final int minutes = ((time.hour - startHour) * 60) + time.minute;
		return (minutes * hourHeight) / 60;
	}

	@Override
	protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int hours = endHour - startHour;

		int width = headerWidth;
		int height = hourHeight * hours;

		setMeasuredDimension(resolveSize(width, widthMeasureSpec), resolveSize(height, heightMeasureSpec));
	}

	private Paint mDividerPaint = new Paint();
	private Paint mLabelPaint = new Paint();

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		final Paint dividerPaint = mDividerPaint;
		dividerPaint.setColor(dividerColor);
		dividerPaint.setStyle(Paint.Style.FILL);

		final Paint labelPaint = mLabelPaint;
		labelPaint.setColor(labelColor);
		labelPaint.setTextSize(labelTextSize);
		labelPaint.setTypeface(Typeface.DEFAULT_BOLD);
		labelPaint.setAntiAlias(true);

		final Paint.FontMetricsInt metrics = labelPaint.getFontMetricsInt();
		final int labelHeight = Math.abs(metrics.ascent);
		final int labelOffset = labelHeight + labelPaddingTop;

		final int right = getRight();

		// Walk left side of canvas drawing timestamps
		final int hours = endHour - startHour;
		for (int i = 0; i < hours; i++) {
			final int dividerY = hourHeight * i;
			final int nextDividerY = hourHeight * (i + 1);
			canvas.drawLine(0, dividerY, right, dividerY, dividerPaint);

			// draw text title for timestamp
			canvas.drawRect(0, dividerY, headerWidth, nextDividerY, dividerPaint);

			// Use hour * hour in miliseconds to get proper value
			String label = DateUtils.formatDateTime(getContext(), (startHour + i)*3600000 + 82800000, DateUtils.FORMAT_SHOW_TIME);
			final float labelWidth = labelPaint.measureText(label);
			canvas.drawText(label, 0, label.length(), headerWidth - labelWidth - labelPaddingLeft, dividerY + labelOffset, labelPaint);
		}
	}

	public int getHeaderWidth() {
		return headerWidth;
	}
}

