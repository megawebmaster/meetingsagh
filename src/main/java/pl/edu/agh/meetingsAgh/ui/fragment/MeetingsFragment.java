package pl.edu.agh.meetingsAgh.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import org.joda.time.LocalDate;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.ui.widget.ObservableScrollView;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MeetingsFragment extends Fragment implements AppFragment, ObservableScrollView.OnScrollListener, ViewPager.OnPageChangeListener {
	private static final String TAG = "MeetingsAGH MeetingsFragment";

	private MeetingPageAdapter adapter;
	private LocalDate currentDate;
	private int currentScrollY = 0;

	public MeetingsFragment(){
		currentDate = new LocalDate();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup)inflater.inflate(R.layout.fragment_meetings, null);

		assert root != null;
		ViewPager pager = (ViewPager)root.findViewById(R.id.meetings_pager);
		adapter = new MeetingPageAdapter(getChildFragmentManager());
		pager.setAdapter(adapter);
		pager.setOnPageChangeListener(this);

		// Add current page
		LocalDate date = new LocalDate();
		MeetingDayFragment first = new MeetingDayFragment(date);
		first.setParentFragment(this);
		adapter.addPage(date, first);

		// Add next page≠
		date = date.plusDays(1);
		MeetingDayFragment second = new MeetingDayFragment(date);
		second.setParentFragment(this);
		adapter.addPage(date, second);

		return root;
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
		// Empty
	}

	@Override
	public void onPageSelected(int position){
		Log.d(TAG, String.format("Selected page: %d", position));
		LocalDate pageDate = currentDate.plusDays(position + 1);
		MeetingDayFragment current = (MeetingDayFragment)adapter.getPage(currentDate.plusDays(position));

		if(!adapter.pageExists(pageDate)){
			Log.d(TAG, "Adding new next page");
			MeetingDayFragment fragment = new MeetingDayFragment(pageDate);
			fragment.setParentFragment(this);
			adapter.addPage(pageDate, fragment);
		}

		// Refresh page
		ScrollView view = current.getScrollView();
		if(view != null){
			view.scrollTo(0, currentScrollY);
		}
	}

	@Override
	public void onPageScrollStateChanged(int state){
		// Empty
	}

	@Override
	public void refresh(){
		if(adapter != null){
			for(Fragment fragment : adapter.getPages()){
				((AppFragment)fragment).refresh();
			}
		}
	}

	@Override
	public void onScrollChanged(ObservableScrollView view){
		// Keep each day view at the same vertical scroll offset.
		currentScrollY = view.getScrollY();
		for(Fragment fragment : adapter.getPages()){
			ObservableScrollView fragmentView = ((MeetingDayFragment)fragment).getScrollView();

			if(fragmentView != null && !fragmentView.equals(view)){
				fragmentView.setOnScrollListener(null);
				fragmentView.scrollTo(0, currentScrollY);
				fragmentView.setOnScrollListener(this);
			}
		}
	}

	/**
	 * Page adapter specially made for meetings calendar.
	 */
	private static class MeetingPageAdapter extends FragmentStatePagerAdapter {
		private LocalDate currentDate;
		private Map<Long, Fragment> fragments = new HashMap<Long, Fragment>();

		public MeetingPageAdapter(android.support.v4.app.FragmentManager fm){
			super(fm);
			currentDate = new LocalDate();
		}

		@Override
		public android.support.v4.app.Fragment getItem(int i){
			Log.d(TAG, String.format("Loading page: %d", i));
			return getPage(currentDate.plusDays(i));
		}

		@Override
		public int getCount(){
			return fragments.size();
		}

		/**
		 * Adds new fragment to the pager.
		 *
		 * @param date Day of new fragment.
		 * @param fragment The fragment.
		 */
		public void addPage(LocalDate date, Fragment fragment){
			fragments.put(date.toDate().getTime(), fragment);
			notifyDataSetChanged();
		}

		/**
		 * Returns fragment based on the page's date.
		 *
		 * @param date The date.
		 * @return Representative fragment or {@code null} if not found.
		 */
		public Fragment getPage(LocalDate date){
			long time = date.toDate().getTime();
			return fragments.get(time);
		}

		/**
		 * Checks if fragment for selected date exists in the pager.
		 *
		 * @param date The date.
		 * @return {@code true} if page exists, {@code false} otherwise.
		 */
		public boolean pageExists(LocalDate date){
			return fragments.containsKey(date.toDate().getTime());
		}

		/**
		 * Returns all current pages of the pager.
		 *
		 * @return Available pages list.
		 */
		public Collection<Fragment> getPages(){
			return fragments.values();
		}
	}
}
