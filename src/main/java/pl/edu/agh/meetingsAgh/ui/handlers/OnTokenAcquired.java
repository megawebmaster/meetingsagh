package pl.edu.agh.meetingsAgh.ui.handlers;

import android.accounts.*;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestListener;
import org.springframework.web.client.HttpClientErrorException;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;

import java.io.IOException;

public class OnTokenAcquired<E> implements AccountManagerCallback<Bundle> {
	private static final String TAG = "MeetingsAGH OnTokenAcquired";
	private final SpiceManager spiceManager;
	private final FragmentActivity parent;
	private final Request<E> request;
	private final RequestListener<E> listener;
	private final RESTCallProgressDialogFragment progress;

	public OnTokenAcquired(FragmentActivity parent, SpiceManager spiceManager, Request<E> request, RequestListener<E> listener,
	                       RESTCallProgressDialogFragment progress){
		this.parent = parent;
		this.spiceManager = spiceManager;
		this.request = request;
		this.listener = listener;
		this.progress = progress;
	}

	@Override
	public void run(AccountManagerFuture<Bundle> future){
		try {
			Bundle result = future.getResult();
			Intent launch = (Intent) result.get(AccountManager.KEY_INTENT);
			if (launch != null) {
				Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
				parent.startActivityForResult(launch, 0);
				return;
			}

			String token = result.getString(AccountManager.KEY_AUTHTOKEN);
			Log.i(TAG, String.format("Auth token: %s", token));

			request.setToken(token);
			spiceManager.execute(request, request.getCacheKey(), DurationInMillis.ALWAYS_EXPIRED, listener);
		} catch(OperationCanceledException e){
			Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
		} catch(IOException e){
			Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
		} catch(AuthenticatorException e){
			Toast.makeText(parent, R.string.unknown_error, Toast.LENGTH_LONG).show();
		} catch(HttpClientErrorException e){
			Log.v(TAG, "Unable to load data for request.");
		}
	}
}