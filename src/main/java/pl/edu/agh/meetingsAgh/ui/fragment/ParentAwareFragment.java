package pl.edu.agh.meetingsAgh.ui.fragment;

import android.support.v4.app.Fragment;

/**
 * Interface for
 */
public interface ParentAwareFragment {
	public void setParentFragment(Fragment parent);
}
