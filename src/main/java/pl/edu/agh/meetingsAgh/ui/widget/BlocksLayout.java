package pl.edu.agh.meetingsAgh.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import pl.edu.agh.meetingsAgh.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Custom layout that contains and organizes a {@link TimeRulerView} and several
 * instances of {@link MeetingButtonView}. Also positions current "now" divider using
 * {@link R.id#blocks_now} view when applicable.
 */
public class BlocksLayout extends ViewGroup {
	private final List<MeetingButtonView> blocks = new LinkedList<MeetingButtonView>();
	private final CopyOnWriteArrayList<MeetingButtonView> blocksToUpdate = new CopyOnWriteArrayList<MeetingButtonView>();

	private TimeRulerView rulerView;
	private View nowView;

	public BlocksLayout(Context context) {
		this(context, null);
	}

	public BlocksLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public BlocksLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BlocksLayout, defStyle, 0);

		assert a != null;
		a.recycle();
	}

	private void ensureChildren() {
		rulerView = (TimeRulerView) findViewById(R.id.blocks_ruler);
		if (rulerView == null) {
			throw new IllegalStateException("Must include a R.id.blocks_ruler view.");
		}
		rulerView.setDrawingCacheEnabled(true);

		nowView = findViewById(R.id.blocks_now);
		if (nowView == null) {
			throw new IllegalStateException("Must include a R.id.blocks_now view.");
		}
		nowView.setDrawingCacheEnabled(true);
	}

	/**
	 * Remove any {@link MeetingButtonView} instances, leaving only
	 * {@link TimeRulerView} remaining.
	 */
	public void removeAllBlocks() {
		ensureChildren();
		removeAllViews();
		blocks.clear();
		addView(rulerView);
		addView(nowView);
	}

	public void addBlock(MeetingButtonView meetingButtonView) {
		meetingButtonView.setDrawingCacheEnabled(true);
		addView(meetingButtonView, 1);
		blocks.add(meetingButtonView);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		ensureChildren();

		rulerView.measure(widthMeasureSpec, heightMeasureSpec);
		nowView.measure(widthMeasureSpec, heightMeasureSpec);

		final int width = rulerView.getMeasuredWidth();
		final int height = rulerView.getMeasuredHeight();

		setMeasuredDimension(resolveSize(width, widthMeasureSpec), resolveSize(height, heightMeasureSpec));
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		ensureChildren();

		final TimeRulerView rulerView = this.rulerView;
		final int headerWidth = rulerView.getHeaderWidth();

		rulerView.layout(0, 0, getWidth(), getHeight());

		for(MeetingButtonView child : blocks){
			final int columnWidth = (getWidth() - headerWidth) / child.getColumns();
			final int top = rulerView.getTimeVerticalOffset(child.getStartTime());
			final int bottom = rulerView.getTimeVerticalOffset(child.getEndTime());
			final int left = headerWidth + (child.getColumn() * columnWidth);
			final int right = left + columnWidth;
			child.layout(left, top, right, bottom);
		}

		// Align now view to match current time
		final View nowView = this.nowView;
		final long now = System.currentTimeMillis();

		final int top = rulerView.getTimeVerticalOffset(now);
		final int bottom = top + nowView.getMeasuredHeight();
		final int left = 0;
		final int right = getWidth();

		nowView.layout(left, top, right, bottom);
	}

	/**
	 * Calculates positions of blocks to display.
	 */
	public void calculatePositions(){
		blocksToUpdate.clear();
		blocksToUpdate.addAll(blocks);

		// Find siblings for each block
		for(MeetingButtonView view : blocks){
			view.setSiblings(getSiblings(view.getStartTime(), view.getEndTime()));
		}

		// Update positions while we have something to update
		while(!blocksToUpdate.isEmpty()){
			for(MeetingButtonView view : blocksToUpdate){
				blocksToUpdate.remove(view);
				List<MeetingButtonView> parents = new ArrayList<MeetingButtonView>();
				parents.add(view);
				updateBlockColumns(parents, view.getSiblings(), view.getStartTime(), view.getEndTime());
			}
		}
	}

	private void updateBlockColumns(List<MeetingButtonView> parents, List<MeetingButtonView> siblings, long startTime, long endTime){
		for(MeetingButtonView sibling : siblings){
			// Narrow time interval for the sibling
			long start = (startTime < sibling.getStartTime()) ? sibling.getStartTime() : startTime;
			long end = (endTime > sibling.getEndTime()) ? sibling.getEndTime() : endTime;
			if(start < end && !parents.contains(sibling)){
				parents.add(sibling);

				int column = 0;
				for(MeetingButtonView item : parents){
					if(item.setColumns(parents.size())){
						item.setColumn(column);
						// Add other siblings to update
						blocksToUpdate.addAllAbsent(item.getSiblings());
						int before = parents.indexOf(item) - 1;
						if(column == 0 || (before > -1 && isBlocksCrossing(item, parents.get(before)))){
							column += 1;
						}
					} else {
						int columns = item.getColumns();
						for(int i = 0; i < columns; i++){
							MeetingButtonView itemInColumn = getItemWithColumn(item.getSiblings(), i);
							if(itemInColumn == null || !isBlocksCrossing(item, itemInColumn)){
								item.setColumn(i);
								column = i+1;
								break;
							}
						}
					}
				}

				// Descent in recursion
				updateBlockColumns(parents, getSiblingsExcept(siblings, sibling), start, end);
				parents.remove(sibling);
			}
		}
	}

	private MeetingButtonView getItemWithColumn(List<MeetingButtonView> items, int column){
		for(MeetingButtonView item : items){
			if(item.getColumn() == column){
				return item;
			}
		}

		return null;
	}


	private boolean isBlocksCrossing(MeetingButtonView item1, MeetingButtonView item2){
		return isBlocksCrossing(item1, item2.getStartTime(), item2.getEndTime());
	}

	private boolean isBlocksCrossing(MeetingButtonView item1, long startTime, long endTime){
		// Check if end time of sibling is between start and end times (starts earlier, but still crosses).
		if(item1.getEndTime() > startTime && item1.getEndTime() < endTime){
			return true;
		}

		// Check if start time of sibling is between start and end times (starts during the block).
		if(item1.getStartTime() >= startTime && item1.getStartTime() < endTime){
			return true;
		}

		// Check if sibling is "around" start and end times.
		return item1.getStartTime() < startTime && item1.getEndTime() > endTime;
	}

	private List<MeetingButtonView> getSiblings(long startTime, long endTime){
		List<MeetingButtonView> siblings = new ArrayList<MeetingButtonView>();

		// Find out how many other siblings needs to fit in the view
		for(MeetingButtonView sibling : blocks){
			if(isBlocksCrossing(sibling, startTime, endTime)){
				siblings.add(sibling);
			}
		}

		return siblings;
	}

	private List<MeetingButtonView> getSiblingsExcept(List<MeetingButtonView> siblings, MeetingButtonView sibling){
		List<MeetingButtonView> result = new ArrayList<MeetingButtonView>(siblings);
		result.remove(sibling);
		return result;
	}
}

