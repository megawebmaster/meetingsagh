package pl.edu.agh.meetingsAgh.ui.handlers;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public  class OnTokenAcquiredError implements Handler.Callback {
	private final Activity parent;

	public OnTokenAcquiredError(FragmentActivity parent){
		this.parent = parent;
	}

	@Override
	public boolean handleMessage(Message msg){
		Toast.makeText(parent, msg.toString(), Toast.LENGTH_LONG).show();
		return false;
	}
}