package pl.edu.agh.meetingsAgh.ui.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.activities.GroupActivity;
import pl.edu.agh.meetingsAgh.adapters.GroupListAdapter;
import pl.edu.agh.meetingsAgh.entity.Group;
import pl.edu.agh.meetingsAgh.entity.GroupList;
import pl.edu.agh.meetingsAgh.request.GroupListRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

public class GroupsFragment extends Fragment implements AppFragment {
	private static final String TAG = "MeetingsAGH GroupFragment";
	private static final String GROUP_LIST = "group_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;

	private GroupListAdapter adapter;
	private GroupList groupsList;

	private int retryCount = 1;

	public GroupsFragment(){
		progress = new RESTCallProgressDialogFragment(GroupList.class, R.string.groups_fragment_loading);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getActivity().getApplicationContext());
		accountsService = new AccountsService(getActivity(), accountManager);
		adapter = new GroupListAdapter(getActivity());

		if(savedInstanceState != null){
			// Restore state
			groupsList = (GroupList)savedInstanceState.getSerializable(GROUP_LIST);
		}

		setHasOptionsMenu(true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putSerializable(GROUP_LIST, groupsList);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup)inflater.inflate(R.layout.fragment_groups, null);

		assert root != null;
		ListView groups = (ListView)root.findViewById(R.id.groups);
		groups.setAdapter(adapter);
		groups.setEmptyView(getActivity().findViewById(android.R.id.empty));

		groups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
				final Group group = (Group)parent.getItemAtPosition(position);
				Intent intent = new Intent(getActivity(), GroupActivity.class);
				intent.putExtra(GroupActivity.GROUP, group);
				getActivity().startActivity(intent);
			}
		});

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getActivity().getApplicationContext());
	}

	@Override
	public void onResume(){
		super.onResume();
		loadGroups();
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.refresh:
				// Remove data to force refresh
				refresh();
				loadGroups();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void refresh(){
		groupsList = null;
	}

	/**
	 * Loads subjects from server if needed.
	 */
	public void loadGroups(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(groupsList == null){
				Request<GroupList> request = new GroupListRequest(accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getFragmentManager());
				accountManager.getAuthToken(account, Constants.AUTHTOKEN_TYPE, new Bundle(), getActivity(),
					new OnTokenAcquired<GroupList>(getActivity(), spiceManager, request, new GroupListRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(getActivity()))
				);
			} else {
				showPage();
			}
		}
	}

	private void showPage(){
		adapter.clear();
		adapter.addAll(groupsList);
		adapter.notifyDataSetChanged();
	}

	private class GroupListRequestListener implements RequestListener<GroupList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadGroups() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadGroups();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(getActivity(), R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(GroupList groups){
			Log.d(TAG, String.format("Groups count: %d", groups.size()));
			progress.dismiss(getFragmentManager());
			groupsList = groups;
			showPage();
		}
	}
}
