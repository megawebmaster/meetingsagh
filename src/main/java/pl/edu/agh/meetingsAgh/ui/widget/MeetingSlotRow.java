package pl.edu.agh.meetingsAgh.ui.widget;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.joda.time.DateTime;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.activities.MeetingActivity;
import pl.edu.agh.meetingsAgh.entity.Meeting;
import pl.edu.agh.meetingsAgh.entity.MeetingReservation;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;
import pl.edu.agh.meetingsAgh.entity.User;
import pl.edu.agh.meetingsAgh.request.MeetingRegisterGroupRequest;
import pl.edu.agh.meetingsAgh.request.MeetingRegisterRequest;
import pl.edu.agh.meetingsAgh.request.MeetingReservationCancelRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.Message;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

import java.util.List;

public class MeetingSlotRow extends GridLayout {
	private static final String TAG = "MeetingsAGH MeetingSlotRow";
	private static final String DATE_FORMAT = "HH:mm";

	private final RESTCallProgressDialogFragment progress;
	private final MeetingActivity parent;
	private final AccountManager accountManager;
	private final AccountsService accountsService;
	private final SpiceManager manager;
	private final Meeting meeting;
	private final MeetingReservation reservation;
	private final int slotNumber;

	private int retryCount = 1;
	private String cancelReason = "";
	private long groupId = 0;
	private boolean isMeetingAuthor;

	public MeetingSlotRow(final MeetingActivity context, final int slotNumber, final long groupId, List<MeetingReservation> reservations){
		super(context);
		parent = context;
		meeting = context.getMeeting();
		manager = context.getSpiceManager();
		accountManager = context.getAccountManager();
		accountsService = context.getAccountsService();
		this.slotNumber = slotNumber;
		this.groupId = groupId;

		float textSize = 16f;
		long startTime = meeting.getStartTime() + slotNumber*meeting.getSlotTime();
		DateTime start = new DateTime(startTime);
		DateTime end = new DateTime(meeting.getStartTime() + (slotNumber+1)*meeting.getSlotTime());

		// Configure the view
		setColumnCount(3);
		int rowMargin = (int)context.getResources().getDimension(R.dimen.meeting_small_margin);
		int slotMargin = (int)context.getResources().getDimension(R.dimen.meeting_slot_margin);

		TextView hour = new TextView(context);
		// Set the text
		hour.setText(String.format("%s - %s", start.toString(DATE_FORMAT), end.toString(DATE_FORMAT)));
		hour.setTextSize(textSize);
		hour.setGravity(Gravity.CENTER);
		addView(hour);

		// Set proper layout parameters
		GridLayout.LayoutParams hourParams = new GridLayout.LayoutParams();
		hourParams.setMargins(rowMargin, slotMargin, rowMargin, slotMargin);
		hourParams.columnSpec = GridLayout.spec(0, 1);
		hour.setLayoutParams(hourParams);

		boolean isRegistered = hasReservation(reservations);
		isMeetingAuthor = accountsService.isCurrentUser(meeting.getAuthor());
		reservation = findReservation(startTime, reservations);
		progress = new RESTCallProgressDialogFragment(MeetingReservationList.class, R.string.meeting_activity_working);

		if(reservation == null && !isRegistered){
			if(isMeetingAuthor){
				// Display "Empty"
				TextView emptySlot = new TextView(context);
				// Set the text
				emptySlot.setText(context.getText(R.string.meeting_slot_empty));
				emptySlot.setTextSize(textSize);
				emptySlot.setGravity(Gravity.CENTER);
				addView(emptySlot);

				// Set proper layout parameters
				GridLayout.LayoutParams emptySlotParams = new GridLayout.LayoutParams();
				emptySlotParams.setMargins(rowMargin, slotMargin, rowMargin, slotMargin);
				emptySlotParams.columnSpec = GridLayout.spec(1, 2);
				emptySlot.setLayoutParams(emptySlotParams);
			} else {
				// Add "Register" button if we are not a meeting author (teacher)
				Button register = new Button(context);
				// Set button text and click listener
				register.setText(context.getText(R.string.meeting_activity_register));
				register.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v){
						showConfirmationDialog();
					}
				});
				register.setGravity(Gravity.CENTER);
				addView(register);

				// Set proper layout parameters
				GridLayout.LayoutParams registerParams = new GridLayout.LayoutParams();
				registerParams.setGravity(Gravity.CENTER);
				registerParams.columnSpec = GridLayout.spec(1, 2);
				register.setLayoutParams(registerParams);
			}
		} else if(reservation != null && (isOwnReservation(reservation) || isMeetingAuthor)) {
			if(isMeetingAuthor){
				// Add registered person name
				TextView person = new TextView(context);
				// Set the text
				person.setText(reservation.getAuthor().toString());
				person.setTextSize(textSize);
				person.setGravity(Gravity.CENTER);
				addView(person);

				// Set proper layout parameters
				GridLayout.LayoutParams personParams = new GridLayout.LayoutParams();
				personParams.setMargins(rowMargin, slotMargin, rowMargin, slotMargin);
				personParams.columnSpec = GridLayout.spec(1, 1);
				person.setLayoutParams(personParams);
			}

			// Add button to cancel reservation
			Button cancel = new Button(context);
			// Set text and click listener
			cancel.setText(context.getText(R.string.cancel));
			cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v){
					showCancelDialog();
				}
			});
			cancel.setGravity(Gravity.CENTER);
			addView(cancel);

			// Set proper layout parameters
			GridLayout.LayoutParams cancelParams = new GridLayout.LayoutParams();
			cancelParams.setGravity(Gravity.CENTER);
			if(isMeetingAuthor){
				cancelParams.columnSpec = GridLayout.spec(2, 1);
			} else {
				cancelParams.columnSpec = GridLayout.spec(1, 2);
			}
			cancel.setLayoutParams(cancelParams);
		}
	}

	private void showConfirmationDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		builder.setMessage(R.string.are_you_sure);
		builder.setTitle(R.string.confirmation);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				executeRegisterRequest();
			}
		});
		builder.setNegativeButton(R.string.no, null);
		builder.create().show();
	}

	private void showCancelDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		final EditText reason = new EditText(parent);

		builder.setMessage(R.string.meeting_slot_cancel);
		builder.setTitle(R.string.confirmation);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which){
				Editable value = reason.getText();
				if(value != null && (value.length() > 0 || isMeetingAuthor)){
					cancelReason = value.toString();
					executeCancelRequest(cancelReason);
				} else {
					Toast.makeText(parent, R.string.meeting_slot_cancel_required, Toast.LENGTH_LONG).show();
				}
			}
		});
		builder.setNegativeButton(R.string.no, null);
		builder.setView(reason);
		builder.create().show();
	}

	private void executeRegisterRequest(){
		if(groupId != MeetingActivity.NO_GROUP){
			executeRegisterGroupRequest(groupId);
		} else {
			executeRegisterStudentRequest();
		}
	}

	private void executeRegisterStudentRequest(){
		// Invoke REST request
		Request<Message> request = new MeetingRegisterRequest(meeting.getId(), slotNumber, accountManager);
		createRequest(request);
	}

	private void executeRegisterGroupRequest(long groupId){
		// Invoke REST request
		Request<Message> request = new MeetingRegisterGroupRequest(meeting.getId(), groupId, slotNumber, accountManager);
		createRequest(request);
	}

	private void createRequest(Request<Message> request){
		progress.setCacheKey(request.getCacheKey());
		progress.show(parent.getSupportFragmentManager());
		accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), parent,
			new OnTokenAcquired<Message>(parent, manager, request, new MeetingRegisterRequestListener(), progress),
			new Handler(new OnTokenAcquiredError(parent))
		);
	}

	private void executeCancelRequest(String reason){
		// Invoke REST request
		Request<Message> request = new MeetingReservationCancelRequest(reservation.getId(), reason, accountManager);
		progress.setCacheKey(request.getCacheKey());
		progress.show(parent.getSupportFragmentManager());
		accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), parent,
			new OnTokenAcquired<Message>(parent, manager, request, new MeetingCancelRequestListener(), progress),
			new Handler(new OnTokenAcquiredError(parent))
		);
	}

	private boolean isOwnReservation(MeetingReservation reservation){
		for(User user : reservation.getStudentList()){
			if(accountsService.isCurrentUser(user)){
				return true;
			}
		}
		return false;
	}

	private boolean hasReservation(List<MeetingReservation> reservations){
		for(MeetingReservation reservation : reservations){
			if(isOwnReservation(reservation)){
				return true;
			}
		}

		return false;
	}

	private MeetingReservation findReservation(long start, List<MeetingReservation> reservations){
		for(MeetingReservation reservation : reservations){
			if(start == reservation.getStartTime()){
				return reservation;
			}
		}

		return null;
	}

	private class MeetingRegisterRequestListener implements RequestListener<Message> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(parent.getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying to register meeting reservation after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				executeRegisterRequest();
			} else {
				// Show "Unable to register"
				Toast.makeText(parent, R.string.meeting_activity_unable_to_register, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(Message message){
			Log.d(TAG, String.format("Message: %s", message.getMessage()));
			progress.dismiss(parent.getSupportFragmentManager());
			// Show "Successfully registered".
			Toast.makeText(parent, message.getMessage(), Toast.LENGTH_LONG).show();

			// If everything is all right - reload parent view.
			if(message.isOk()){
				parent.reloadMeeting();
			}
		}
	}

	private class MeetingCancelRequestListener implements RequestListener<Message> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(parent.getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying to cancel meeting reservation after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				executeCancelRequest(cancelReason);
			} else {
				// Show "Unable to register"
				Toast.makeText(parent, R.string.meeting_activity_unable_to_cancel, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(Message message){
			Log.d(TAG, String.format("Message: %s", message.getMessage()));
			progress.dismiss(parent.getSupportFragmentManager());
			// Show "Successfully cancelled".
			Toast.makeText(parent, message.getMessage(), Toast.LENGTH_LONG).show();

			// If everything is all right - reload parent view.
			if(message.isOk()){
				parent.reloadMeeting();
			}
		}
	}
}
