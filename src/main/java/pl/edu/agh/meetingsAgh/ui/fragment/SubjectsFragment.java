package pl.edu.agh.meetingsAgh.ui.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.activities.SubjectActivity;
import pl.edu.agh.meetingsAgh.adapters.SubjectListAdapter;
import pl.edu.agh.meetingsAgh.entity.Subject;
import pl.edu.agh.meetingsAgh.entity.SubjectList;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.request.SubjectListRequest;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

public class SubjectsFragment extends Fragment implements AppFragment {
	private static final String TAG = "MeetingsAGH SubjectFragment";
	private static final String SUBJECT_LIST = "subject_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;

	private SubjectListAdapter adapter;
	private SubjectList subjectList;

	private int retryCount = 1;

	public SubjectsFragment(){
		progress = new RESTCallProgressDialogFragment(SubjectList.class, R.string.subjects_fragment_loading);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getActivity().getApplicationContext());
		accountsService = new AccountsService(getActivity(), accountManager);
		adapter = new SubjectListAdapter(getActivity());

		if(savedInstanceState != null){
			// Restore state
			subjectList = (SubjectList)savedInstanceState.getSerializable(SUBJECT_LIST);
		}

		setHasOptionsMenu(true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putSerializable(SUBJECT_LIST, subjectList);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup)inflater.inflate(R.layout.fragment_subjects, null);

		assert root != null;
		ListView subjects = (ListView)root.findViewById(R.id.subjects);
		subjects.setAdapter(adapter);
		subjects.setEmptyView(getActivity().findViewById(android.R.id.empty));

		subjects.setOnItemClickListener(new AdapterView.OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
				final Subject subject = (Subject)parent.getItemAtPosition(position);
				Intent intent = new Intent(getActivity(), SubjectActivity.class);
				intent.putExtra(SubjectActivity.SUBJECT, subject);
				getActivity().startActivity(intent);
			}
		});

		return root;
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getActivity().getApplicationContext());
	}

	@Override
	public void onResume(){
		super.onResume();
		loadSubjects();
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.refresh:
				// Remove data to force refresh
				refresh();
				loadSubjects();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void refresh(){
		subjectList = null;
	}

	/**
	 * Loads subjects from server if needed.
	 */
	public void loadSubjects(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(subjectList == null){
				Request<SubjectList> request = new SubjectListRequest(accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getFragmentManager());
				accountManager.getAuthToken(account, Constants.AUTHTOKEN_TYPE, new Bundle(), getActivity(),
					new OnTokenAcquired<SubjectList>(getActivity(), spiceManager, request, new SubjectListRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(getActivity()))
				);
			} else {
				showPage();
			}
		}
	}

	private void showPage(){
		adapter.clear();
		adapter.addAll(subjectList);
		adapter.notifyDataSetChanged();
	}

	private class SubjectListRequestListener implements RequestListener<SubjectList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadSubjects() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadSubjects();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(getActivity(), R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(SubjectList subjects){
			Log.d(TAG, String.format("Subjects count: %d", subjects.size()));
			progress.dismiss(getFragmentManager());
			subjectList = subjects;
			showPage();
		}
	}
}
