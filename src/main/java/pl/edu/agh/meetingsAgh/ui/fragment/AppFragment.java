package pl.edu.agh.meetingsAgh.ui.fragment;

public interface AppFragment {
	/**
	 * Calls fragment to refresh its content.
	 */
	public void refresh();
}
