package pl.edu.agh.meetingsAgh.entity;

import java.io.Serializable;
import java.util.List;

public class Subject implements Serializable {
	private Long id;
	private String name;
	private boolean open;
	private boolean teacher;
	private List<SubjectUser> teacherList;
	private List<SubjectUser> studentList;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isOpen() {
		return open;
	}

	public List<SubjectUser> getTeacherList() {
		return teacherList;
	}

	public List<SubjectUser> getStudentList() {
		return studentList;
	}

	public boolean isTeacher() {
		return teacher;
	}

	@Override
	public String toString(){
		return getName();
	}
}
