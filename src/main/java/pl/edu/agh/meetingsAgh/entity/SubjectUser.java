package pl.edu.agh.meetingsAgh.entity;

public class SubjectUser extends User {
	private Type type;
	private boolean banned;
	private boolean hasGroup;

	public Type getType() {
		return type;
	}

	public boolean isBanned() {
		return banned;
	}

	public boolean isHasGroup() {
		return hasGroup;
	}

	public static enum Type {
		STUDENT, TEACHER, OWNER;
	}
}
