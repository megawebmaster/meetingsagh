package pl.edu.agh.meetingsAgh.entity;

import java.io.Serializable;
import java.util.List;

public class Meeting implements Serializable {
	private Long id;
	private long startTime;
	private long endTime;
	private String registrationTo;
	private String description;
	private Long freeSlots;
	private Long slots;
	private Long usersPerSlot;
	private Type type;
	private boolean sticky;
	private User author;
	private String place;
	private List<Subject> subjectList;
	private String color;

	public Long getId() {
		return id;
	}

	public long getStartTime(){
		return startTime;
	}

	public long getEndTime(){
		return endTime;
	}

	public String getRegistrationTo() {
		return registrationTo;
	}

	public String getDescription() {
		return description;
	}

	public Long getFreeSlots() {
		return freeSlots;
	}

	public Long getSlots() {
		return slots;
	}

	public long getSlotTime(){
		return (endTime - startTime)/slots;
	}

	public Long getUsersPerSlot() {
		return usersPerSlot;
	}

	public Type getType() {
		return type;
	}

	public boolean isSticky() {
		return sticky;
	}

	public User getAuthor() {
		return author;
	}

	public String getPlace() {
		return place;
	}

	public String getColor() {
		return color;
	}

	public List<Subject> getSubjectList() {
		return subjectList;
	}

	public static enum Type {
		VISIBLE, INVISIBLE;
	}
}
