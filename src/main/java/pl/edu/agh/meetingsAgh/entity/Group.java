package pl.edu.agh.meetingsAgh.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Group implements Serializable {
	private Long id;
	private String name;
	private String description;
	private Subject subject;
	private UserGroup.Status status;
	private List<UserGroup> studentList;

	public Long getId(){
		return id;
	}

	public String getName(){
		return name;
	}

	public String getDescription(){
		return description;
	}

	public Subject getSubject(){
		return subject;
	}

	public UserGroup.Status getStatus(){
		return status;
	}

	public List<UserGroup> getStudentList(){
		ArrayList<UserGroup> students = new ArrayList<UserGroup>();
		for(UserGroup student : studentList){
			if(student.getStatus() == UserGroup.Status.ACCEPTED){
				students.add(student);
			}
		}

		return students;
	}
}
