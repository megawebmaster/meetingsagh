package pl.edu.agh.meetingsAgh.entity;

import java.io.Serializable;
import java.util.List;

public class MeetingReservation implements Serializable {
	private Long id;
	private long startTime;
	private long endTime;
	private String description;
	private Status status;
	private Type type;
	private User author;
	private User mainTeacher;
	private boolean team;
	private boolean teacher;
	private String cancelReason;
	private String color;
	private List<Subject> subjectList;
	private List<User> studentList;

	public Long getId(){
		return id;
	}

	public long getStartTime(){
		return startTime;
	}

	public long getEndTime(){
		return endTime;
	}

	public String getDescription(){
		return description;
	}

	public Status getStatus(){
		return status;
	}

	public User getAuthor(){
		return author;
	}

	public User getMainTeacher(){
		return mainTeacher;
	}

	public boolean isTeacher(){
		return teacher;
	}

	public List<Subject> getSubjectList(){
		return subjectList;
	}

	public boolean isTeam(){
		return team;
	}

	public Type getType(){
		return type;
	}

	public List<User> getStudentList(){
		return studentList;
	}

	public String getCancelReason(){
		return cancelReason;
	}

	public String getColor(){
		return color;
	}

	public static enum Status {
		OPEN, CANCELED;
	}

	public static enum Type {
		VISIBLE, INVISIBLE;
	}
}
