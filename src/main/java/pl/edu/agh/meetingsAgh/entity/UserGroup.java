package pl.edu.agh.meetingsAgh.entity;

public class UserGroup extends User {
	private Status status;

	public Status getStatus(){
		return status;
	}

	public enum Status {
		ACCEPTED, INVITED, REJECTED
	}
}
