package pl.edu.agh.meetingsAgh.entity;

import java.io.Serializable;

public class User implements Serializable {
	private Long id;
	private String firstName;
	private String lastName;
	private String login;
	private String email;
	private boolean admin;
	private boolean teacher;
	private boolean student;

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getLogin() {
		return login;
	}

	public String getEmail() {
		return email;
	}

	public boolean isAdmin() {
		return admin;
	}

	public boolean isTeacher() {
		return teacher;
	}

	public boolean isStudent() {
		return student;
	}

	/**
	 * Returns currently assigned role to the user.
	 *
	 * @return User's role.
	 */
	public Role getRole(){
		if(isAdmin()){
			return Role.ADMIN;
		}

		if(isTeacher()){
			return Role.TEACHER;
		}

		return Role.STUDENT;
	}

	public String toString(){
		return String.format("%s %s", firstName, lastName);
	}

	public enum Role {
		ADMIN, TEACHER, STUDENT
	}
}
