package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.MeetingList;

import java.util.Date;

public class MeetingListRequest extends Request<MeetingList> {
	private static final String TAG = "MeetingsAGH MeetingListRequest";
	private static final String JSON_CACHE_KEY = "meeting_list_%d";
	private final Date start;
	private final Date end;
	private final AccountManager accountManager;

	public MeetingListRequest(Date start, Date end, AccountManager accountManager) {
		super(MeetingList.class);
		this.start = start;
		this.end = end;
		this.accountManager = accountManager;
		setRetryPolicy(new RetryPolicy<MeetingList>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, start.getTime()+end.getTime());
	}

	@Override
	public MeetingList loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/meeting/list/?start=%d&end=%d&access_token=%s",
			Constants.SERVICE_URI, start.getTime()/1000, end.getTime()/1000, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, MeetingList.class);
	}
}