package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.SubjectList;

public class SubjectListRequest extends Request<SubjectList> {
	private static final String TAG = "MeetingsAGH SubjectListRequest";
	private static final String JSON_CACHE_KEY = "subject_list";

	public SubjectListRequest(AccountManager accountManager) {
		super(SubjectList.class);
		setRetryPolicy(new RetryPolicy<SubjectList>(accountManager, this));
	}

	@Override
	public SubjectList loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/subject/list/?access_token=%s", Constants.SERVICE_URI, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, SubjectList.class);
	}

	@Override
	public String getCacheKey(){
		return JSON_CACHE_KEY;
	}
}