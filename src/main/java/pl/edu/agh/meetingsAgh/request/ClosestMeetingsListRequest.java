package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.MeetingList;

public class ClosestMeetingsListRequest extends Request<MeetingList> {
	private static final String TAG = "MeetingsAGH ClosestMeetingsListRequest";
	private static final String JSON_CACHE_KEY = "closest_meetings_list_%d";
	private final long id;
	private final AccountManager accountManager;

	public ClosestMeetingsListRequest(long id, AccountManager accountManager) {
		super(MeetingList.class);
		this.id = id;
		this.accountManager = accountManager;
		setRetryPolicy(new RetryPolicy<MeetingList>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, id);
	}

	@Override
	public MeetingList loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/subject/%d/meetings/?count=%d&access_token=%s",
			Constants.SERVICE_URI, id, Constants.CLOSEST_MEETINGS_COUNT, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, MeetingList.class);
	}
}