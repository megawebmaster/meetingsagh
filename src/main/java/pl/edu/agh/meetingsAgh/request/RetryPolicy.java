package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import pl.edu.agh.meetingsAgh.Constants;

public class RetryPolicy<E> implements com.octo.android.robospice.retry.RetryPolicy {
	private final Request<E> request;
	private final AccountManager manager;

	public RetryPolicy(AccountManager manager, Request<E> request){
		this.request = request;
		this.manager = manager;
	}

	@Override
	public int getRetryCount(){
		return 0;
	}

	@Override
	public void retry(SpiceException e){
		manager.invalidateAuthToken(Constants.ACCOUNT_TYPE, request.getToken());
	}

	@Override
	public long getDelayBeforeRetry(){
		return 0;
	}
}