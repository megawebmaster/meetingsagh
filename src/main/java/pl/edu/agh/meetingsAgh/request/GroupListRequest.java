package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.GroupList;

public class GroupListRequest extends Request<GroupList> {
	private static final String TAG = "MeetingsAGH GroupListRequest";
	private static final String JSON_CACHE_KEY = "group_list";

	public GroupListRequest(AccountManager accountManager) {
		super(GroupList.class);
		setRetryPolicy(new RetryPolicy<GroupList>(accountManager, this));
	}

	@Override
	public GroupList loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/team/list/?access_token=%s", Constants.SERVICE_URI, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, GroupList.class);
	}

	@Override
	public String getCacheKey(){
		return JSON_CACHE_KEY;
	}
}