package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.Meeting;

public class MeetingRequest extends Request<Meeting> {
	private static final String TAG = "MeetingsAGH MeetingRequest";
	private static final String JSON_CACHE_KEY = "meeting_%d";

	private final long id;
	private final AccountManager accountManager;

	public MeetingRequest(long id, AccountManager accountManager) {
		super(Meeting.class);
		this.id = id;
		this.accountManager = accountManager;
		setRetryPolicy(new RetryPolicy<Meeting>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, id);
	}

	@Override
	public Meeting loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/meeting/%d/?access_token=%s",
			Constants.SERVICE_URI, id, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, Meeting.class);
	}
}
