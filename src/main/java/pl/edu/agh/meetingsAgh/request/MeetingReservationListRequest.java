package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;

public class MeetingReservationListRequest extends Request<MeetingReservationList> {
	private static final String TAG = "MeetingsAGH MeetingReservationListRequest";
	private static final String JSON_CACHE_KEY = "meeting_slot_list_%d";
	private final long id;

	public MeetingReservationListRequest(long id, AccountManager accountManager) {
		super(MeetingReservationList.class);
		this.id = id;
		setRetryPolicy(new RetryPolicy<MeetingReservationList>(accountManager, this));
	}

	@Override
	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, id);
	}

	@Override
	public MeetingReservationList loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/meeting/%d/slots/?access_token=%s", Constants.SERVICE_URI, id, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, MeetingReservationList.class);
	}
}