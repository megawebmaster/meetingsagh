package pl.edu.agh.meetingsAgh.request;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public abstract class Request<E> extends SpringAndroidSpiceRequest<E> {
	protected String accessToken;

	public Request(Class<E> clazz){
		super(clazz);
	}

	public String getToken(){
		return accessToken;
	}

	public void setToken(String accessToken){
		this.accessToken = accessToken;
	}

	public abstract String getCacheKey();
}
