package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.ui.Message;

public class MeetingEditRequest extends Request<Message> {
	private static final String TAG = "MeetingsAGH MeetingEditRequest";
	private static final String JSON_CACHE_KEY = "meeting_edit_%d";
	private final long id;
	private final String place;
	private final String description;

	public MeetingEditRequest(long id, String place, String description, AccountManager accountManager) {
		super(Message.class);
		this.id = id;
		this.place = place;
		this.description = description;
		setRetryPolicy(new RetryPolicy<Message>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, id);
	}

	@Override
	public Message loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/meeting/%d/edit/?access_token=%s",
			Constants.SERVICE_URI, id, accessToken);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.set("place", place);
		parameters.set("description", description);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);
		RestTemplate template = getRestTemplate();
		template.getMessageConverters().add(new FormHttpMessageConverter());

		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return template.postForObject(uri, request, Message.class);
	}
}