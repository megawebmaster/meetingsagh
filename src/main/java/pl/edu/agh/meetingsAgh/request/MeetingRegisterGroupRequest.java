package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.ui.Message;

public class MeetingRegisterGroupRequest extends Request<Message> {
	private static final String TAG = "MeetingsAGH MeetingRegisterGroupRequest";
	private static final String JSON_CACHE_KEY = "meeting_register_group_%d_%d";
	private final long id;
	private final long groupId;
	private final long slot;

	public MeetingRegisterGroupRequest(long id, long groupId, long slot, AccountManager accountManager) {
		super(Message.class);
		this.id = id;
		this.groupId = groupId;
		this.slot = slot;
		setRetryPolicy(new RetryPolicy<Message>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY, id, slot);
	}

	@Override
	public Message loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/meeting/%d/team/%d/signin/%d/?access_token=%s",
			Constants.SERVICE_URI, id, groupId, slot, accessToken);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);
		RestTemplate template = getRestTemplate();
		template.getMessageConverters().add(new FormHttpMessageConverter());

		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return template.postForObject(uri, request, Message.class);
	}
}