package pl.edu.agh.meetingsAgh.request;

import android.accounts.AccountManager;
import android.util.Log;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.entity.User;

public class UserRequest extends Request<User> {
	private static final String TAG = "MeetingsAGH UserRequest";
	private static final String JSON_CACHE_KEY = "user";

	public UserRequest(AccountManager accountManager) {
		super(User.class);
		setRetryPolicy(new RetryPolicy<User>(accountManager, this));
	}

	public String getCacheKey(){
		return String.format(JSON_CACHE_KEY);
	}

	@Override
	public User loadDataFromNetwork() throws Exception {
		String uri = String.format("http://%s/rest/user/?access_token=%s",
			Constants.SERVICE_URI, accessToken);
		Log.d(TAG, String.format("Request URI: %s\n", uri));
		return getRestTemplate().getForObject(uri, User.class);
	}
}
