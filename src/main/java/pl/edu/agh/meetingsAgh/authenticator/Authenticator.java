package pl.edu.agh.meetingsAgh.authenticator;

import android.accounts.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.activities.AuthenticatorActivity;

import java.io.IOException;

public class Authenticator extends AbstractAccountAuthenticator {
	public static final String USER_ROLE = "user_role";

	/** The tag used to log to adb console. **/
	private static final String TAG = "MeetingsAGH Authenticator";

	// Authentication Service context
	private final Context context;

	public Authenticator(Context context){
		super(context);
		this.context = context;
	}

	@Override
	public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options){
		Log.v(TAG, "addAccount()");
		final Intent intent = new Intent(context, AuthenticatorActivity.class);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
		final Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		return bundle;
	}

	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options){
		Log.v(TAG, "confirmCredentials()");
		return null;
	}

	@Override
	public Bundle editProperties(AccountAuthenticatorResponse response, String accountType){
		Log.v(TAG, "editProperties()");
		throw new UnsupportedOperationException();
	}

	@Override
	public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle loginOptions) throws NetworkErrorException {
		Log.v(TAG, "getAuthToken()");

		if (!authTokenType.equals(Constants.AUTHTOKEN_TYPE)) {
			final Bundle result = new Bundle();
			result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
			return result;
		}

		// Extract the username and password from the Account Manager, and ask
		// the server for an appropriate AuthToken.
		final AccountManager am = AccountManager.get(context);
		assert am != null;
		final String password = am.getPassword(account);
		if(password != null){
			Log.d(TAG, "Fetching access token");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(String.format("http://%s/oauth/token?grant_type=password&client_id=%s&client_secret=%s&username=%s&password=%s",
				Constants.SERVICE_URI, Constants.OAUTH_CLIENT_ID, Constants.OAUTH_CLIENT_SECRET, account.name, password));
			HttpResponse httpResponse;
			try {
				httpResponse = httpClient.execute(httpPost);
				String responseString = EntityUtils.toString(httpResponse.getEntity());
				final JSONObject authenticationObject = new JSONObject(responseString);
				final String accessToken = authenticationObject.getString("access_token");
				Log.d(TAG, "Received access token: "+accessToken);

				if (!TextUtils.isEmpty(accessToken)) {
					final Bundle result = new Bundle();
					result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
					result.putString(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
					result.putString(AccountManager.KEY_AUTHTOKEN, accessToken);
					return result;
				}
			} catch(IOException e){
				final Bundle result = new Bundle();
				result.putString(AccountManager.KEY_ERROR_MESSAGE, "Network error or JSON parsing error.");
				return result;
			} catch(JSONException e) {
				final Bundle result = new Bundle();
				result.putString(AccountManager.KEY_ERROR_MESSAGE, "Network error or JSON parsing error.");
				return result;
			}
		}

		// If we get here, then we couldn't access the user's password - so we
		// need to re-prompt them for their credentials. We do that by creating
		// an intent to display our AuthenticatorActivity panel.
		final Intent intent = new Intent(context, AuthenticatorActivity.class);
		intent.putExtra(AuthenticatorActivity.PARAM_USERNAME, account.name);
		intent.putExtra(AuthenticatorActivity.PARAM_AUTHTOKEN_TYPE, authTokenType);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
		final Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		return bundle;
	}

	@Override
	public String getAuthTokenLabel(String authTokenType){
		// null means we don't support multiple authToken types
		Log.v(TAG, "getAuthTokenLabel()");
		return null;
	}

	@Override
	public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features){
		// This call is used to query whether the Authenticator supports
		// specific features. We don't expect to get called, so we always
		// return false (no) for any queries.
		Log.v(TAG, "hasFeatures()");
		final Bundle result = new Bundle();
		result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
		return result;
	}

	@Override
	public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle loginOptions){
		Log.v(TAG, "updateCredentials()");
		return null;
	}
}
