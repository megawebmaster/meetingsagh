package pl.edu.agh.meetingsAgh.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.authenticator.Authenticator;
import pl.edu.agh.meetingsAgh.entity.User;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.request.UserRequest;

public class AuthenticatorActivity extends AccountAuthenticatorActivity {
	/** The Intent extra to store username. */
	public static final String PARAM_USERNAME = "username";

	/** The Intent extra to store authentication token type. */
	public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";

	/** The tag used to log to adb console. */
	private static final String TAG = "MeetingsAGH AuthenticatorActivity";

	/** Keep track of the progress dialog so we can dismiss it */
	private final DialogFragment progress = new ProgressDialogFragment();

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);

	private AccountManager accountManager;

	/** Keep track of the login task so can cancel it if requested */
	private UserLoginTask authTask = null;

	private TextView message;

	private EditText passwordEdit;

	private EditText usernameEdit;

	private boolean requestNewAccount;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		accountManager = AccountManager.get(this);

		final Intent intent = getIntent();
		String username = intent.getStringExtra(PARAM_USERNAME);
		this.requestNewAccount = username == null;

		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.login_activity);
		getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_alert);

		message = (TextView)findViewById(R.id.message);
		usernameEdit = (EditText)findViewById(R.id.username_edit);
		passwordEdit = (EditText)findViewById(R.id.password_edit);

		if(!TextUtils.isEmpty(username)){
			usernameEdit.setText(username);
		}
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();

		final Intent intent = new Intent();
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_CANCELED, intent);
		finish();
	}

	@Override
	protected void onStart(){
		super.onStart();
		spiceManager.start(getApplicationContext());
	}

	@Override
	protected void onStop(){
		super.onStop();
		spiceManager.shouldStop();
	}

	/**
	 * Handles onClick event on the Submit button. Sends username/password to
	 * the server for authentication. The button is configured to call
	 * handleLogin() in the layout XML.
	 *
	 * @param view The Submit button for which this method is invoked
	 */
	public void handleLogin(View view) {
		String username = usernameEdit.getText().toString();
		String password = passwordEdit.getText().toString();

		if (TextUtils.isEmpty(username)) {
			// If no username, then we ask the user to log in using an
			// appropriate service.
			message.setText(getText(R.string.login_activity_loginfail_text_credentials));
			return;
		}
		if (TextUtils.isEmpty(password)) {
			// We have an account but no password
			message.setText(getText(R.string.login_activity_loginfail_text_pwmissing));
			return;
		}
		// Show a progress dialog, and kick off a background task to perform the user login_activity attempt.
		progress.show(getFragmentManager(), "progress");
		authTask = new UserLoginTask();
		authTask.execute(username, password);
	}

	/**
	 * Called when response is received from the server for authentication
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller. We store the
	 * authToken that's returned from the server as the 'password' for this
	 * account - so we're never storing the user's actual password locally.
	 */
	private void finishLogin(String username, String password, User.Role role){
		final Account account = new Account(username, Constants.ACCOUNT_TYPE);

		// Put user's role in account data
		Bundle userdata = new Bundle();
		userdata.putString(Authenticator.USER_ROLE, role.toString());

		if(requestNewAccount){
			accountManager.addAccountExplicitly(account, password, userdata);
		} else {
			accountManager.setPassword(account, password);
		}

		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, username);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}

	/**
	 * Called when the authentication process completes (see attemptLogin()).
	 *
	 * @param authenticationObject the authentication object returned by server.
	 */
	public void onAuthenticationResult(JSONObject authenticationObject) {
		Log.i(TAG, "onAuthenticationResult()");
		if(authenticationObject != null){
			try {
				String accessToken = authenticationObject.getString("access_token");
				String username = authenticationObject.getString("username");
				String password = authenticationObject.getString("password");
				Log.d(TAG, "  Has proper access token: "+accessToken);

				// Fetch user information
				Request<User> request = new UserRequest(accountManager);
				request.setToken(accessToken);
				spiceManager.execute(request, request.getCacheKey(), DurationInMillis.ALWAYS_EXPIRED, new UserRequestListener(username, password));
			} catch(JSONException e) {
				// "Authentication failed, check your credentials."
				message.setText(getText(R.string.login_activity_loginfail_auth_failed));
			}
		} else {
			// "Server down, check your internet connection"
			message.setText(getText(R.string.login_activity_loginfail_server_down));
		}

		// Our task is complete, so clear it out
		authTask = null;
	}

	public void onAuthenticationCancel() {
		Log.i(TAG, "onAuthenticationCancel()");
		// Our task is complete, so clear it out
		authTask = null;
		// Hide the progress dialog
		progress.dismiss();
	}

	private class UserRequestListener implements RequestListener<User> {
		private final String username;
		private final String password;

		public UserRequestListener(String username, String password){
			this.username = username;
			this.password = password;
		}

		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss();
			// Show "Request failed"
			Toast.makeText(AuthenticatorActivity.this, R.string.request_failure, Toast.LENGTH_LONG).show();
		}

		@Override
		public void onRequestSuccess(User user){
			Log.d(TAG, String.format("User loaded: %s", user.getLogin()));
			progress.dismiss();
			finishLogin(username, password, user.getRole());
		}
	}

	/**
	 * Represents an asynchronous task used to authenticate a user against the
	 * SampleSync Service
	 */
	private class UserLoginTask extends AsyncTask<String, Void, JSONObject> {
		private static final String TAG = "MeetingsUserLoginTask";

		@Override
		protected JSONObject doInBackground(String... params){
			if(params.length != 2){
				Log.e(TAG, "UserLoginTask.doInBackground: invalid use of user login task, needs to provide username and password.");
				return null;
			}

			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(String.format("http://%s/oauth/token?grant_type=password&client_id=%s&client_secret=%s&username=%s&password=%s",
					Constants.SERVICE_URI, Constants.OAUTH_CLIENT_ID, Constants.OAUTH_CLIENT_SECRET, params[0], params[1]));
				HttpResponse response = httpClient.execute(httpPost);
				String responseString = EntityUtils.toString(response.getEntity());
				if(!TextUtils.isEmpty(responseString)){
					JSONObject object = new JSONObject(responseString);
					// Pass username and password entered by user.
					object.put("username", params[0]);
					object.put("password", params[1]);
					return object;
				}
			} catch (Exception ex) {
				Log.e(TAG, "UserLoginTask.doInBackground: failed to authenticate");
				Log.i(TAG, ex.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(final JSONObject authenticationObject) {
			onAuthenticationResult(authenticationObject);
		}

		@Override
		protected void onCancelled() {
			onAuthenticationCancel();
		}
	}

	private static class ProgressDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle args) {
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setMessage(getText(R.string.login_activity_authenticating));
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			return dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog){
			super.onCancel(dialog);

			Log.i(TAG, "user cancelling authentication");
			UserLoginTask authTask = ((AuthenticatorActivity)getActivity()).authTask;
			if(authTask != null){
				authTask.cancel(true);
			}
		}
	}
}
