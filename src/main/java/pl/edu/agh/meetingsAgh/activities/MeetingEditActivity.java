package pl.edu.agh.meetingsAgh.activities;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.Meeting;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;
import pl.edu.agh.meetingsAgh.request.MeetingEditRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.Message;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

/**
 * Activity which displays edit view for a meeting.
 */
public class MeetingEditActivity extends FragmentActivity {
	public static final String MEETING = "meeting";

	private static final String TAG = "MeetingsAGH MeetingEditActivity";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;

	private Meeting meeting;
	private EditText placeText;
	private EditText descriptionText;

	private int retryCount = 1;

	public MeetingEditActivity(){
		progress = new RESTCallProgressDialogFragment(MeetingReservationList.class, R.string.meeting_activity_loading);
	}

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getApplicationContext());
		accountsService = new AccountsService(this, accountManager);
		setContentView(R.layout.meeting_edit);

		Intent intent = getIntent();
		Meeting meeting = (Meeting)intent.getSerializableExtra(MEETING);
		assert meeting != null;
		this.meeting = meeting;

		placeText = (EditText)findViewById(R.id.place);
		descriptionText = (EditText)findViewById(R.id.description);

		setSubject();
		setPlace();
		setDescription();
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getApplicationContext());
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	public Meeting getMeeting(){
		return meeting;
	}

	public AccountManager getAccountManager(){
		return accountManager;
	}

	/**
	 * Handles onClick event on the Cancel button.
	 *
	 * @param view The Submit button for which this method is invoked
	 */
	public void handleCancel(View view){
		finish();
	}

	/**
	 * Handles onClick event on the Save button.
	 *
	 * @param view The Submit button for which this method is invoked
	 */
	public void handleSave(View view){
		executeEditRequest(placeText.getText().toString(), descriptionText.getText().toString());
	}

	private void executeEditRequest(String place, String description){
		// Invoke REST request
		Request<Message> request = new MeetingEditRequest(meeting.getId(), place, description, accountManager);
		progress.setCacheKey(request.getCacheKey());
		progress.show(getSupportFragmentManager());
		accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
			new OnTokenAcquired<Message>(this, spiceManager, request, new MeetingEditListener(), progress),
			new Handler(new OnTokenAcquiredError(this))
		);
	}

	private void setSubject(){
		TextView subject = (TextView)findViewById(R.id.subject);
		String title;

		if(meeting.getSubjectList().isEmpty()){
			title = meeting.getAuthor().toString();
		} else {
			title = Joiner.on(", ").join(meeting.getSubjectList());
		}

		subject.setText(String.format("%s - %s", title, DateUtils.formatDateTime(this, meeting.getStartTime(), DateUtils.FORMAT_SHOW_DATE)));
	}

	private void setPlace(){
		placeText.setText(meeting.getPlace());
	}

	private void setDescription(){
		descriptionText.setText(meeting.getDescription());
	}

	private class MeetingEditListener implements RequestListener<Message> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying to edit meeting after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				executeEditRequest(placeText.getText().toString(), descriptionText.getText().toString());
			} else {
				// Show "Unable to register"
				Toast.makeText(MeetingEditActivity.this, R.string.meeting_edit_activity_unable_to_edit, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(Message message){
			Log.d(TAG, String.format("Message: %s", message.getMessage()));
			progress.dismiss(getSupportFragmentManager());
			// Show "Successfully edited".
			Toast.makeText(MeetingEditActivity.this, message.getMessage(), Toast.LENGTH_LONG).show();

			// If everything is all right - close meeting view.
			if(message.isOk()){
				Intent intent = new Intent();
				intent.putExtra("refresh", true);
				MeetingEditActivity.this.setResult(RESULT_OK, intent);
				MeetingEditActivity.this.finish();
			}
		}
	}
}