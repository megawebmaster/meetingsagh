package pl.edu.agh.meetingsAgh.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.adapters.ClosestMeetingsAdapter;
import pl.edu.agh.meetingsAgh.entity.MeetingList;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;
import pl.edu.agh.meetingsAgh.entity.Subject;
import pl.edu.agh.meetingsAgh.request.ClosestMeetingsListRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

/**
 * List adapter created to display list of subjects.
 */
public class SubjectActivity extends FragmentActivity {
	public static final String SUBJECT = "subject";

	private static final String TAG = "MeetingsAGH SubjectActivity";
	private static final String CLOSEST_MEETINGS_LIST = "closest_meetings_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;
	private ClosestMeetingsAdapter adapter;

	private Subject subject;
	private MeetingList closestMeetings;

	private int retryCount = 1;

	public SubjectActivity(){
		progress = new RESTCallProgressDialogFragment(MeetingReservationList.class, R.string.subject_activity_loading);
	}

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getApplicationContext());
		accountsService = new AccountsService(this, accountManager);
		setContentView(R.layout.subject);

		// Prepare list of closest meetings
		adapter = new ClosestMeetingsAdapter(this);
		ListView items = (ListView)findViewById(R.id.items);
		items.setAdapter(adapter);
		items.setEmptyView(findViewById(android.R.id.empty));

		// Prepare subject view
		Intent intent = getIntent();
		Subject subject = (Subject)intent.getSerializableExtra(SUBJECT);
		assert subject != null;
		this.subject = subject;

		if(savedInstanceState != null){
			// Restore state
			closestMeetings = (MeetingList)savedInstanceState.getSerializable(CLOSEST_MEETINGS_LIST);
		}

		setSubject();
		setTeacher();
	}

	@Override
	protected void onResume(){
		super.onResume();
		loadSubject();
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getApplicationContext());
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putSerializable(CLOSEST_MEETINGS_LIST, closestMeetings);
	}

	public AccountManager getAccountManager(){
		return accountManager;
	}

	/**
	 * Loads meeting data.
	 *
	 * Queries server for data if needed.
	 */
	public void loadSubject(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(closestMeetings == null){
				Request<MeetingList> request = new ClosestMeetingsListRequest(subject.getId(), accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getSupportFragmentManager());
				accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
					new OnTokenAcquired<MeetingList>(this, spiceManager, request, new ClosestMeetingsRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(this))
				);
			} else {
				loadClosestMeetings();
			}
		}
	}

	private void setSubject(){
		TextView subject = (TextView)findViewById(R.id.subject);
		subject.setText(this.subject.getName());
	}

	private void setTeacher(){
		TextView date = (TextView)findViewById(R.id.teacher);
		date.setText(String.format(getText(R.string.subject_activity_teacher).toString(), Joiner.on(", ").join(subject.getTeacherList())));
	}

	private void loadClosestMeetings(){
		// For each slot
		adapter.clear();
		adapter.addAll(closestMeetings);
		adapter.notifyDataSetChanged();
	}

	private class ClosestMeetingsRequestListener implements RequestListener<MeetingList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadSubject() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadSubject();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(SubjectActivity.this, R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(MeetingList meetings){
			Log.d(TAG, String.format("Closest meetings: %d", meetings.size()));
			progress.dismiss(getSupportFragmentManager());
			closestMeetings = meetings;
			loadClosestMeetings();
		}
	}
}