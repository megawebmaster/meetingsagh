package pl.edu.agh.meetingsAgh.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.authenticator.Authenticator;
import pl.edu.agh.meetingsAgh.entity.User;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.fragment.AppFragment;
import pl.edu.agh.meetingsAgh.ui.fragment.GroupsFragment;
import pl.edu.agh.meetingsAgh.ui.fragment.MeetingsFragment;
import pl.edu.agh.meetingsAgh.ui.fragment.SubjectsFragment;

public class MainActivity extends FragmentActivity {
	public static final int REFRESH_MEETING_DAY = 100;
	private static final String TAG = "MeetingsAGH MainActivity";
	private static final String MEETINGS_TAB = "meetings_tab";
	private static final String SUBJECTS_TAB = "subjects_tab";
	private static final String GROUPS_TAB = "groups_tab";
	private static final String CURRENT_TAB = "current_tab";

	private AccountsService accountsService;
	private ActionBar.Tab groupsTab;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		accountsService = new AccountsService(this, AccountManager.get(this));
		ActionBar actionBar = getActionBar();
		assert actionBar != null;
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Add tabs
		FragmentManager fragmentManager = getSupportFragmentManager();
		Log.d(TAG, "Adding Meetings tab");
		ActionBar.Tab meetingsTab = actionBar.newTab().setText(R.string.tabs_meetings)
			.setTabListener(new TabListener(fragmentManager, MEETINGS_TAB, MeetingsFragment.class));
		actionBar.addTab(meetingsTab);

		Log.d(TAG, "Adding Subjects tab");
		ActionBar.Tab subjectsTab = actionBar.newTab().setText(R.string.tabs_subjects)
			.setTabListener(new TabListener(fragmentManager, SUBJECTS_TAB, SubjectsFragment.class));
		actionBar.addTab(subjectsTab);

		// Select current tab
		if(savedInstanceState != null) {
			actionBar.setSelectedNavigationItem(savedInstanceState.getInt(CURRENT_TAB));
		}
	}

	@Override
	protected void onStart(){
		super.onStart();
		accountsService.selectAccount();
		Account account = accountsService.getAccount();

		if(account != null){
			hideTabs(account);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		ActionBar actionBar = getActionBar();
		assert actionBar != null;
		// Save current tab
		outState.putInt(CURRENT_TAB, actionBar.getSelectedNavigationIndex());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		switch(requestCode){
			case AccountsService.REQUEST_PICK_ACCOUNT:
				if(resultCode == RESULT_OK){
					String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
					if(!accountsService.getCurrentUser().equals(accountName)){
						accountsService.setAccount(accountName);

						// Refresh attached fragments
						AppFragment fragment = (AppFragment)getSupportFragmentManager().findFragmentByTag(MEETINGS_TAB);
						if(fragment != null){
							fragment.refresh();
						}

						fragment = (AppFragment)getSupportFragmentManager().findFragmentByTag(SUBJECTS_TAB);
						if(fragment != null){
							fragment.refresh();
						}

						hideTabs(accountsService.getAccount());

						fragment = (AppFragment)getSupportFragmentManager().findFragmentByTag(GROUPS_TAB);
						if(fragment != null){
							fragment.refresh();
						}
					}

				} else if(accountsService.getAccount() == null) {
					finish();
				}
				break;
			case REFRESH_MEETING_DAY:
				if(data != null && data.getBooleanExtra("refresh", false)){
					AppFragment fragment = (AppFragment)getSupportFragmentManager().findFragmentByTag(MEETINGS_TAB);
					if(fragment != null){
						fragment.refresh();
					}
				}
				break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.select_account:
				accountsService.showAccountSelector();
				return true;
			// TODO: Logout as account removing?
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private boolean hasTab(ActionBar actionBar, ActionBar.Tab groupsTab){
		for(int i = 0; i < actionBar.getTabCount(); i++){
			if(actionBar.getTabAt(i).equals(groupsTab)){
				return true;
			}
		}

		return false;
	}

	private void hideTabs(Account account){
		AccountManager manager = AccountManager.get(getApplicationContext());
		ActionBar actionBar = getActionBar();

		boolean hasTab = hasTab(actionBar, groupsTab);
		boolean isStudent = manager.getUserData(account, Authenticator.USER_ROLE).equals(User.Role.STUDENT.toString());

		if(isStudent && !hasTab){
			Log.d(TAG, "Adding Groups tab");
			groupsTab = actionBar.newTab().setText(R.string.tabs_groups)
				.setTabListener(new TabListener(getSupportFragmentManager(), GROUPS_TAB, GroupsFragment.class));
			actionBar.addTab(groupsTab);
		} else if(!isStudent && hasTab){
			Log.d(TAG, "Remove Groups tab");
			actionBar.removeTab(groupsTab);
		}
	}

	private class TabListener implements ActionBar.TabListener {
		private final FragmentManager manager;
		private final String tag;
		private final Class<? extends Fragment> clazz;
		private Fragment fragment;

		public TabListener(FragmentManager manager, String tag, Class<? extends Fragment> clazz){
			this.manager = manager;
			this.tag = tag;
			this.clazz = clazz;
		}

		@Override
		public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft){
			Log.d(TAG, String.format("Selected tab: %s, position: %d", tab.getText(), tab.getPosition()));
			android.support.v4.app.FragmentTransaction transaction = MainActivity.this.getSupportFragmentManager().beginTransaction();

			// Replace currently visible fragment
			if((fragment = manager.findFragmentByTag(tag)) == null){
				fragment = Fragment.instantiate(MainActivity.this, clazz.getName());
				transaction.add(R.id.main_pager, fragment, tag);
			} else {
				transaction.attach(fragment);
			}

			transaction.commit();
		}

		@Override
		public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft){
			Log.d(TAG, String.format("Unselected tab: %s", tab.getText()));
			android.support.v4.app.FragmentTransaction transaction = MainActivity.this.getSupportFragmentManager().beginTransaction();
			transaction.detach(fragment);
			transaction.commit();
		}

		@Override
		public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft){
			Log.d(TAG, String.format("Reselected tab: %s", tab.getText()));
		}
	}
}
