package pl.edu.agh.meetingsAgh.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.adapters.ClosestMeetingsAdapter;
import pl.edu.agh.meetingsAgh.entity.Group;
import pl.edu.agh.meetingsAgh.entity.MeetingList;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;
import pl.edu.agh.meetingsAgh.entity.UserGroup;
import pl.edu.agh.meetingsAgh.request.ClosestMeetingsListRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;

/**
 * List adapter created to display list of subjects.
 */
public class GroupActivity extends FragmentActivity {
	public static final String GROUP = "group";

	private static final String TAG = "MeetingsAGH GroupActivity";
	private static final String CLOSEST_MEETINGS_LIST = "group_closest_meetings_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;
	private ClosestMeetingsAdapter adapter;

	private Group group;
	private MeetingList closestMeetings;

	private int retryCount = 1;

	public GroupActivity(){
		progress = new RESTCallProgressDialogFragment(MeetingReservationList.class, R.string.group_activity_loading);
	}

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getApplicationContext());
		accountsService = new AccountsService(this, accountManager);
		setContentView(R.layout.group);

		// Prepare group view
		Intent intent = getIntent();
		Group group = (Group)intent.getSerializableExtra(GROUP);
		assert group != null;
		this.group = group;

		// Prepare list of closest meetings
		adapter = new ClosestMeetingsAdapter(this, group.getId());
		ListView items = (ListView)findViewById(R.id.items);
		items.setAdapter(adapter);
		items.setEmptyView(findViewById(android.R.id.empty));

		if(savedInstanceState != null){
			// Restore state
			closestMeetings = (MeetingList)savedInstanceState.getSerializable(CLOSEST_MEETINGS_LIST);
		}

		setGroup();
		setSubject();
		setTeacher();
		setStudents();
	}

	@Override
	protected void onResume(){
		super.onResume();
		loadGroup();
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getApplicationContext());
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putSerializable(CLOSEST_MEETINGS_LIST, closestMeetings);
	}

	public AccountManager getAccountManager(){
		return accountManager;
	}

	/**
	 * Loads meeting data.
	 *
	 * Queries server for data if needed.
	 */
	public void loadGroup(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(closestMeetings == null){
				Request<MeetingList> request = new ClosestMeetingsListRequest(group.getSubject().getId(), accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getSupportFragmentManager());
				accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
					new OnTokenAcquired<MeetingList>(this, spiceManager, request, new ClosestMeetingsRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(this))
				);
			} else {
				loadClosestMeetings();
			}
		}
	}

	private void setGroup(){
		TextView groupName = (TextView)findViewById(R.id.group);
		groupName.setText(this.group.getName());
	}

	private void setStudents(){
		ListView students = (ListView)findViewById(R.id.students);
		ArrayAdapter<UserGroup> studentsAdapter = new ArrayAdapter<UserGroup>(getApplicationContext(), R.layout.group_student_item);
		students.setAdapter(studentsAdapter);
		studentsAdapter.addAll(group.getStudentList());
		studentsAdapter.notifyDataSetChanged();
	}

	private void setSubject(){
		TextView subject = (TextView)findViewById(R.id.subject);
		subject.setText(String.format(getText(R.string.group_activity_subject).toString(), this.group.getSubject().getName()));
	}

	private void setTeacher(){
		TextView teacher = (TextView)findViewById(R.id.teacher);
		teacher.setText(String.format(getText(R.string.group_activity_teacher).toString(), Joiner.on(", ")
			.useForNull(getText(R.string.none).toString()).join(group.getSubject().getTeacherList())));
	}

	private void loadClosestMeetings(){
		// For each slot
		adapter.clear();
		adapter.addAll(closestMeetings);
		adapter.notifyDataSetChanged();
	}

	private class ClosestMeetingsRequestListener implements RequestListener<MeetingList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadGroup() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadGroup();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(GroupActivity.this, R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(MeetingList meetings){
			Log.d(TAG, String.format("Closest meetings: %d", meetings.size()));
			progress.dismiss(getSupportFragmentManager());
			closestMeetings = meetings;
			loadClosestMeetings();
		}
	}
}