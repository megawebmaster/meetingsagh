package pl.edu.agh.meetingsAgh.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import pl.edu.agh.meetingsAgh.Constants;
import pl.edu.agh.meetingsAgh.R;
import pl.edu.agh.meetingsAgh.entity.Meeting;
import pl.edu.agh.meetingsAgh.entity.MeetingReservationList;
import pl.edu.agh.meetingsAgh.request.MeetingCancelRequest;
import pl.edu.agh.meetingsAgh.request.MeetingRequest;
import pl.edu.agh.meetingsAgh.request.MeetingReservationListRequest;
import pl.edu.agh.meetingsAgh.request.Request;
import pl.edu.agh.meetingsAgh.services.AccountsService;
import pl.edu.agh.meetingsAgh.ui.Message;
import pl.edu.agh.meetingsAgh.ui.fragment.RESTCallProgressDialogFragment;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquired;
import pl.edu.agh.meetingsAgh.ui.handlers.OnTokenAcquiredError;
import pl.edu.agh.meetingsAgh.ui.widget.MeetingSlotRow;

/**
 * Activity which displays single meeting.
 */
public class MeetingActivity extends FragmentActivity {
	public static final int REFRESH_MEETING_DAY = 100;
	public static final int NO_GROUP = 0;
	public static final String MEETING = "meeting";
	public static final String GROUP = "group_id";

	private static final String TAG = "MeetingsAGH MeetingActivity";
	private static final String MEETINGS_RESERVATIONS_LIST = "meetings_reservations_list";

	private final SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
	private final RESTCallProgressDialogFragment progress;

	private AccountsService accountsService;
	private AccountManager accountManager;
	private TableLayout items;

	private MeetingReservationList meetingReservations;
	private Meeting meeting;
	private long groupId;

	private int retryCount = 1;
	private String cancelReason = "";

	public MeetingActivity(){
		progress = new RESTCallProgressDialogFragment(Meeting.class, R.string.meeting_edit_activity_loading);
	}

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getApplicationContext());
		accountsService = new AccountsService(this, accountManager);
		setContentView(R.layout.meeting);

		items = (TableLayout)findViewById(R.id.items);
		Intent intent = getIntent();
		Meeting meeting = (Meeting)intent.getSerializableExtra(MEETING);
		assert meeting != null;
		this.meeting = meeting;
		this.groupId = intent.getLongExtra(GROUP, NO_GROUP);

		if(savedInstanceState != null){
			// Restore state
			meetingReservations = (MeetingReservationList)savedInstanceState.getSerializable(MEETINGS_RESERVATIONS_LIST);
		}

		setSubject();
		setDate();
		setPlace();
		setDescription();
	}

	@Override
	public void onStart() {
		super.onStart();
		spiceManager.start(getApplicationContext());
		loadMeeting();
	}

	@Override
	public void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Save current tab
		outState.putSerializable(MEETINGS_RESERVATIONS_LIST, meetingReservations);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		// If user is meeting author add new menu positions
		if(getAccountsService().isCurrentUser(meeting.getAuthor())){
			inflater.inflate(R.menu.meeting, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.cancel:
				showCancelDialog();
				return true;
			case R.id.edit:
				Intent intent = new Intent(this, MeetingEditActivity.class);
				intent.putExtra(MeetingEditActivity.MEETING, meeting);
				startActivityForResult(intent, MainActivity.REFRESH_MEETING_DAY);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		switch(requestCode){
			case REFRESH_MEETING_DAY:
				if(data != null && data.getBooleanExtra("refresh", false)){
					executeLoadMeetingRequest();
				}
				break;
		}
	}

	public Meeting getMeeting(){
		return meeting;
	}

	public SpiceManager getSpiceManager(){
		return spiceManager;
	}

	public AccountsService getAccountsService(){
		return accountsService;
	}

	public AccountManager getAccountManager(){
		return accountManager;
	}

	/**
	 * Loads meeting data.
	 *
	 * Queries server for data if needed.
	 */
	public void loadMeeting(){
		Account account = accountsService.getAccount();
		if(account != null){
			if(meetingReservations == null){
				Request<MeetingReservationList> request = new MeetingReservationListRequest(meeting.getId(), accountManager);
				progress.setCacheKey(request.getCacheKey());
				progress.show(getSupportFragmentManager());
				accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
					new OnTokenAcquired<MeetingReservationList>(this, spiceManager, request, new MeetingSlotListRequestListener(), progress),
					new Handler(new OnTokenAcquiredError(this))
				);
			} else {
				loadTable();
			}
		}
	}

	/**
	 * Forces to reload meeting data from server.
	 */
	public void reloadMeeting(){
		meetingReservations = null;
		loadMeeting();
	}

	private void showCancelDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText reason = new EditText(this);

		builder.setMessage(R.string.meeting_activity_cancel);
		builder.setTitle(R.string.confirmation);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which){
				Editable value = reason.getText();
				if(value != null && value.length() > 0){
					cancelReason = value.toString();
				}

				executeCancelRequest(cancelReason);
			}
		});
		builder.setNegativeButton(R.string.no, null);
		builder.setView(reason);
		builder.create().show();
	}

	private void executeCancelRequest(String reason){
		// Invoke REST request
		Request<Message> request = new MeetingCancelRequest(meeting.getId(), reason, accountManager);
		progress.setCacheKey(request.getCacheKey());
		progress.show(getSupportFragmentManager());
		accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
			new OnTokenAcquired<Message>(this, spiceManager, request, new MeetingCancelListener(), progress),
			new Handler(new OnTokenAcquiredError(this))
		);
	}

	private void executeLoadMeetingRequest(){
		// Invoke REST request
		Request<Meeting> request = new MeetingRequest(meeting.getId(), accountManager);
		progress.setCacheKey(request.getCacheKey());
		progress.show(getSupportFragmentManager());
		accountManager.getAuthToken(accountsService.getAccount(), Constants.AUTHTOKEN_TYPE, new Bundle(), this,
			new OnTokenAcquired<Meeting>(this, spiceManager, request, new MeetingListener(), progress),
			new Handler(new OnTokenAcquiredError(this))
		);
	}

	private void setSubject(){
		TextView subject = (TextView)findViewById(R.id.subject);
		if(meeting.getSubjectList().isEmpty()){
			subject.setText(meeting.getAuthor().toString());
		} else {
			subject.setText(Joiner.on(", ").join(meeting.getSubjectList()));
		}
	}

	private void setDate(){
		TextView date = (TextView)findViewById(R.id.date);
		date.setText(DateUtils.formatDateTime(this, meeting.getStartTime(), DateUtils.FORMAT_SHOW_DATE));
	}

	private void setPlace(){
		TextView place = (TextView)findViewById(R.id.place);
		place.setText(String.format(getText(R.string.meeting_activity_place).toString(), meeting.getPlace()));
	}

	private void setDescription(){
		TextView description = (TextView)findViewById(R.id.description);
		description.setText(String.format(getText(R.string.meeting_activity_description).toString(), meeting.getDescription()));
	}

	private void loadTable(){
		// For each slot
		items.removeAllViews();
		for(int i = 0; i < meeting.getSlots(); i++){
			items.addView(new MeetingSlotRow(this, i, groupId, meetingReservations));
		}
	}

	private class MeetingSlotListRequestListener implements RequestListener<MeetingReservationList> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying loadMeeting() after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				loadMeeting();
			} else {
				// Show "Request failed"
				// TODO: Maybe reask for the password here?
				Toast.makeText(MeetingActivity.this, R.string.request_failure, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(MeetingReservationList slots){
			Log.d(TAG, String.format("Meeting slots: %d", slots.size()));
			progress.dismiss(getSupportFragmentManager());
			meetingReservations = slots;
			loadTable();
		}
	}

	private class MeetingCancelListener implements RequestListener<Message> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying to cancel meeting after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				executeCancelRequest(cancelReason);
			} else {
				// Show "Unable to register"
				Toast.makeText(MeetingActivity.this, R.string.meeting_activity_unable_to_cancel, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(Message message){
			Log.d(TAG, String.format("Message: %s", message.getMessage()));
			progress.dismiss(getSupportFragmentManager());
			// Show "Successfully cancelled".
			Toast.makeText(MeetingActivity.this, message.getMessage(), Toast.LENGTH_LONG).show();

			// If everything is all right - close meeting view.
			if(message.isOk()){
				Intent intent = new Intent();
				intent.putExtra("refresh", true);
				MeetingActivity.this.setResult(RESULT_OK, intent);
				MeetingActivity.this.finish();
			}
		}
	}

	private class MeetingListener implements RequestListener<Meeting> {
		@Override
		public void onRequestFailure(SpiceException spiceException){
			progress.dismiss(getSupportFragmentManager());

			if(retryCount > 0){
				Log.d(TAG, "Retrying to cancel meeting after failure.");
				// Token was invalidated so we need to retry to download meetings.
				retryCount -= 1;
				executeCancelRequest(cancelReason);
			} else {
				// Show "Unable to register"
				Toast.makeText(MeetingActivity.this, R.string.meeting_activity_unable_to_cancel, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onRequestSuccess(Meeting meeting){
			Log.d(TAG, String.format("Meeting ID: %d", meeting.getId()));
			progress.dismiss(getSupportFragmentManager());
			MeetingActivity.this.meeting = meeting;
			setSubject();
			setDate();
			setPlace();
			setDescription();
		}
	}
}